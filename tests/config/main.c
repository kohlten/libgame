//
// Created by kohlten on 12/7/18.
//

#include "config.h"
#include "assert.h"
#include "stdio.h"
#include "vector.h"
#include "libft.h"
#include <SDL.h>

/*
WINDOW_SIZE 1080,720
WINDOW_COLOR 0,0,0,255
WINDOW_FULLSCREEN FALSE
WINDOW_SHOWN TRUE
WINDOW_HIDDEN FALSE
WINDOW_BORDERLESS FALSE
WINDOW_RESIZABLE FALSE
WINDOW_MINIMISED FALSE
WINDOW_MAXIMISED TRUE
WINDOW_ALLOW_HIGHDPI TRUE
WINDOW_ON_TOP TRUE
WINDOW_SKIP_TASKBAR FALSE

# RENDERER
RENDER_SOFTWARE FALSE
RENDER_ACCELERATED TRUE
RENDER_PRESENT_VSYNC TRUE
*/

int main() {
    t_config config;
    t_vector2f size;
    SDL_Color color;
    float num;
    bool b;

    assert(init_config(&config, "config.txt", 0) == CONFIG_SUCCESS, "Failed to init config!\n");

    assert(get_value_config_bool(&config, "TEST_BOOL0", &b) == CONFIG_SUCCESS, "Failed to get node!\n");
    assert(b == true, "FAILED TO GET BOOL\n");
    assert(get_value_config_bool(&config, "TEST_BOOL1", &b) == CONFIG_SUCCESS, "Failed to get node!\n");
    assert(b == false, "FAILED TO GET BOOL\n");

    assert(get_value_config_num(&config, "TEST_NUM0", &num) == CONFIG_SUCCESS, "Failed to get node!\n");
    assert(num == 1, "FAILED TO GET NUM\n");
    assert(get_value_config_num(&config, "TEST_NUM1", &num) == CONFIG_SUCCESS, "Failed to get node!\n");
    printf("%f\n", num);
    assert(get_value_config_num(&config, "TEST_NUM2", &num) == CONFIG_SUCCESS, "Failed to get node!\n");
    printf("%f\n", num);
    assert(get_value_config_num(&config, "TEST_NUM3", &num) == CONFIG_SUCCESS, "Failed to get node!\n");
    printf("%f\n", num);
    assert(get_value_config_num(&config, "TEST_NUM4", &num) == CONFIG_SUCCESS, "Failed to get node!\n");
    printf("%f\n", num);
    assert(get_value_config_num(&config, "TEST_NUM5", &num) == CONFIG_SUCCESS, "Failed to get node!\n");
    printf("%f\n", num);

    assert(get_value_config_color(&config, "TEST_COLOR0", &color) == CONFIG_SUCCESS, "Failed to get node!\n");
    assert(color.r == 255 && color.g == 155 && color.b == 255 && color.a == 255, "FAILED TO GET COLOR\n");

    assert(get_value_config_vector(&config, "TEST_VECTOR20", &size) == CONFIG_SUCCESS, "Failed to get node!\n");
    assert(size.x == 255 && size.y == 255, "FAILED TO GET VECTOR\n");
    free_config(&config);
    return (0);
}
