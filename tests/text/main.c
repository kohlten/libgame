#include "rendering/text/text.h"
#include "window.h"
#include "fps.h"
#include "libft.h"

#include <stdio.h>

void render_texts(t_text **texts, t_window *window)
{
    for (int i = 0; i < 4; i++)
        render_text(texts[i], window->SDLrenderer);
}

t_window *init()
{
	t_window *window;

	if (SDL_Init(SDL_INIT_VIDEO) < 0)
    {
        printf("SDL cout_ld not initialize! SDL_Error: %s\n", SDL_GetError());
        return NULL;
    }
    window = new_window(350, 200, "Text test");
    if (!window)
    {
        printf("Could not malloc the window!\n");
        return NULL;
    }
    return (window);
}

t_text **init_texts(t_window *window) {
    t_text **texts;

    texts = ft_memalloc(4 * sizeof(t_text *));
    if (!texts)
        return (NULL);
    for (int i = 0; i < 4; i++) {
        texts[i] = new_text("0", window->SDLrenderer, (SDL_Color) {255, 255, 255, 255}, (t_vector2i) {0, i * 30},
                            "../../fonts/freemono_regular_20.json", NULL);
        if (!texts[i])
            return (NULL);
    }
    change_text_string(texts[0], window->SDLrenderer, "Hello world!");
    change_text_string(texts[1], window->SDLrenderer, "1234567890");
    change_text_string(texts[2], window->SDLrenderer, "~`!@#$%^&*()-_+={}'");
    change_text_string(texts[3], window->SDLrenderer, "|[]\\\\:\";'<>?,./");
    return texts;
}

void free_texts(t_text **texts, int amount)
{
    for (int i = 0; i < amount; i++)
        free_text(texts[i]);
    free(texts);
}

int main()
{
    t_window *window;
    int done = 0;
    int frames = 0;
    SDL_Event e;
    t_text **texts;

    window = init();
    if (!window)
        return (-1);
    texts = init_texts(window);
    if (!texts)
        return (-1);
    while (!done && frames < 250) {
        limit_fps(60);
    	while (SDL_PollEvent(&e)) {
            if (e.type == SDL_QUIT)
                done = 1;
        }
        SDL_SetRenderDrawColor(window->SDLrenderer, 0, 0, 0, 255);
        SDL_RenderClear(window->SDLrenderer);
        render_texts(texts, window);
        SDL_RenderPresent(window->SDLrenderer);
        frames++;
    }
    free_window(window);
    free_texts(texts, 4);
    return (0);
}