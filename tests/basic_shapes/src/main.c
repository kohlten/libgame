//
// Created by kohlten on 1/5/19.
//

#include "window.h"
#include "rendering/basic_shapes.h"

t_window *init() {
    t_window *window;

    if (SDL_Init(SDL_INIT_VIDEO) < 0)
    {
        printf("SDL cout_ld not initialize! SDL_Error: %s\n", SDL_GetError());
        return NULL;
    }
    window = new_window((SDL_Rect) {SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 1080, 720}, "Basic Shapes Test", 0, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
    if (!window)
        return NULL;
    return (window);
}

int main() {
    t_window *window;
    int done = 0;
    int frames = 0;
    SDL_Event e;

    window = init();
    if (!window)
        return (-1);
    while (!done) {
        while (SDL_PollEvent(&e)) {
            if (e.type == SDL_QUIT)
                done = 1;
            if (e.type == SDL_KEYDOWN && e.key.keysym.sym == SDLK_ESCAPE)
                done = 1;
        }
        SDL_SetRenderDrawColor(window->SDLrenderer, 0, 0, 0, 255);
        SDL_RenderClear(window->SDLrenderer);
        draw_circle(new_vector2i(100, 100), 50, true, (SDL_Color){255, 0, 0, 255}, window->SDLrenderer);
        SDL_RenderPresent(window->SDLrenderer);
        frames++;
    }
    free_window(window);
    return (0);
}