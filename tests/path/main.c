#include "game_io.h"
#include <stdio.h>
#include "libft.h"

void assert(int bool, char *message) {
    if (!bool) {
        printf("%s", message);
        exit(-1);
    }
}

int main()
{
    char *str;

    str = get_path_of_file("/var/www/balh/blah/test.txt");
    assert(ft_strcmp(str, "/var/www/balh/blah/") == 0, "Failed on basic test\n");
    free(str);
    str = get_path_of_file("./filename");
    assert(ft_strcmp(str, "./") == 0, "Failed on ./\n");
    free(str);
    str = get_path_of_file("filaname");
    assert(str == NULL, "Failed on just filename\n");
    free(str);
    return (0);
}

