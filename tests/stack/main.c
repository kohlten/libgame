//
// Created by Alexander Strole on 12/3/18.
//

#include "data_structures/stack.h"
#include <stdio.h>
#include "assert.h"
#include "libft.h"

int main()
{
    t_stack *stack;
    t_linked_list *data;
    t_linked_list **datas;

    stack = new_stack();
    assert(stack != NULL, "Failed to allocated stack!\n");
    assert(get_len_stack(stack) == 0, "Length of stack is incorrect!\n");
    datas = ft_memalloc(sizeof(t_linked_list *) * 10);
    for (int i = 0; i < 10; i++) {
        data = new_node_linked_list(NULL, NULL, NULL);
        assert(data != NULL, "Failed to allocate data!\n");
        datas[i] = data;
        push_stack(stack, data);
        assert(get_len_stack(stack) == i + 1, "Length of stack is incorrect when pushing!\n");
    }
    for (int i = 0; i < 10; i++) {
        data = pop_stack(stack);
        assert(data != NULL || datas[i] != data, "Failed to get data back!");
        free(data);
    }
    free(datas);
    free_stack(stack, 1, NULL);
    return (0);
}

