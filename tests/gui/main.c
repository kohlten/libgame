//
// Created by Alexander Strole on 12/11/18.
//

#include "gui/gui.h"
#include "window.h"
#include "fps.h"
#include "libft.h"
#include <stdio.h>

t_window *init()
{
    t_window *window;

    if (SDL_Init(SDL_INIT_VIDEO) < 0)
    {
        printf("SDL cout_ld not initialize! SDL_Error: %s\n", SDL_GetError());
        return NULL;
    }
    window = new_window(1080, 720, "GUI test", 0, 0);
    if (!window)
        return NULL;
    return (window);
}

int main()
{
    t_gui menu;
    t_window *window;
    int done = 0;
    int frames = 0;
    SDL_Event e;
    gui_error code;

    window = init();
    if (!window)
        return (-1);
    code = init_gui(&menu, "../../gui_configs/main_menu/main_menu.json", window->SDLrenderer);
    if (code != GUI_SUCCESS)
    {
        printf("Failed to parse gui with code: %d\n", code);
        return (-1);
    }
    while (!done && frames < 250) {
        limit_fps(60);
        while (SDL_PollEvent(&e)) {
            if (e.type == SDL_QUIT)
                done = 1;
        }
        SDL_SetRenderDrawColor(window->SDLrenderer, 0, 0, 0, 255);
        SDL_RenderClear(window->SDLrenderer);
        display_gui(&menu, window->SDLrenderer);
        SDL_RenderPresent(window->SDLrenderer);
        frames++;
    }
    free_window(window);
    return (0);

}