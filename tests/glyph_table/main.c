#include "rendering/text/text_bitmap/glyph_table.h"
#include <stdio.h>

#define PROFILE_CODE

int basic_test(t_glyph_table *table)
{
	char *string = "Hello world!";
	t_glyph_node *node;
	glyph_table_error error;

	for (int i = 0; string[i]; i++)
	{
		error = get_glyph_table_node(table, string[i], &node);
		if (error != GLYPH_SUCCESS)
			return -1;
	}
	return 0;
}

int main()
{
	t_glyph_table *table;
	glyph_table_error error;

	table = new_glyph_table(0);
	error = set_glyph_table_values(table, "../../fonts/freemono_regular_20.json");
	if (error != GLYPH_SUCCESS)
		return -1;
	if (basic_test(table) != 0)
		return -1;
	free_glyph_table(table, NULL);
}