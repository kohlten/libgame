#include "data_structures/hashtable.h"
#include "libft.h"
#include <stdio.h>

typedef struct
{
	int i;
} node;

int basic_test(t_hashtable *table)
{
	char *loc;
	node *data;
	hashtable_error code;

	for (int i = 0; i < table->size; i++)
	{
		loc = ft_itoa(i);
		data = ft_memalloc(sizeof(node));
		data->i = i;
		code = add_node_hashtable(table, loc, data);
		if (code != HASH_SUCCESS)
		{
			printf("FAILURE: Failed to add node!\n");
			return -1;
		}
		free(loc);
	}
	for (int i = 0; i < table->size; i++)
	{
		loc = ft_itoa(i);
		code = get_node_hashtable(table, loc, (void *)&data);
		if (code != HASH_SUCCESS || data->i != i)
		{
			printf("FAILURE: I got %d; I expected: %d\n", data->i, i);
			free_hashtable(table, true, NULL);
			return -1;
		}
		free(loc);
	}
	return 0;
}

int not_found_test(t_hashtable *table)
{
	node *data;
	hashtable_error code;

	code = get_node_hashtable(table, "BAD_NODE", (void *)&data);
	if (code != HASH_NOT_FOUND)
	{
		printf("FAILURE: I got a node when I wasen't supposed to!\n");
		return -1;
	}
	code = remove_node_hashtable(table, "BAD_NODE", true, NULL);
	if (code != HASH_NOT_FOUND)
	{
		printf("FAILURE: I got a node when I wasen't supposed to!\n");
		return -1;
	}
	return 0;
}

int duplicate_test(t_hashtable *table)
{
	hashtable_error code;
	node *n;

	n = ft_memalloc(sizeof(node));
	code = add_node_hashtable(table, "10", n);
	if (code != HASH_DUPLICATE_DATA)
	{
		printf("FAILURE: Was able to double assign!\n");
		return -1;
	}
	free(n);
	return 0;
}

int remove_test(t_hashtable *table)
{
	hashtable_error code;
	node *data;
	char *loc;

	for (int i = 0; i < table->size / 2; i++)
	{
		loc = ft_itoa(i);
		code = remove_node_hashtable(table, loc, true, NULL);
		if (code != HASH_SUCCESS)
		{
			printf("FAILURE: I failed to remove the node!\n");
			return -1;
		}
		code = get_node_hashtable(table, loc, (void *)&data);
		if (code != HASH_NOT_FOUND)
		{
			printf("FAILURE: I failed to remove the node!\n");
			return -1;
		}
		free(loc);
	}
	return 0;
}

int resize_test(t_hashtable *table)
{
	char *loc;
	node *data;
	hashtable_error code;

	for (int i = 5000; i < 6000; i++)
	{
		loc = ft_itoa(i);
		data = ft_memalloc(sizeof(node));
		data->i = i;
		code = add_node_hashtable(table, loc, data);
		if (code != HASH_SUCCESS)
		{
			printf("FAILURE: Failed to add node!\n");
			return -1;
		}
		if (table->nodes > table->size)
		{	
			code = resize_hashtable(table, table->size * 2);
			if (code != HASH_SUCCESS)
				return -1;
		}
		free(loc);
	}
	return 0;
}

int set_test(t_hashtable *hashtable)
{
    char *key;
    node *data;
    node *new_data;
    node *new_node;
    hashtable_error code;

    key = ft_strnew(5);
    ft_strcpy(key, "Hello");
    data = ft_memalloc(sizeof(node));
    data->i = 1;
    code = add_node_hashtable(hashtable, key, data);
    if (code != HASH_SUCCESS)
        return -1;
    new_data = ft_memalloc(sizeof(node));
    new_data->i = 2;
    code = set_node_hastable(hashtable, key, new_data, 1, NULL);
    if (code != HASH_SUCCESS)
        return -1;
    code = get_node_hashtable(hashtable, key, (void **)&new_node);
    if (code != HASH_SUCCESS)
        return -1;
    if (new_node != new_data || new_node->i != new_data->i)
        return -1;
    return 0;
}

void print_lengths(t_hashtable *table)
{
	int *lengths;
	hashtable_error code;

	code = get_node_lengths_hashtable(table, &lengths);
	if (code != HASH_SUCCESS)
		return;
	for (int i = 0; i < table->size; i++)
	{
		if (i % 100 == 0)
			printf("\n");
		printf("%d ", lengths[i]);
	}
	free(lengths);
}

int main()
{
	t_hashtable table;
	bool failure = false;

	init_hashtable(&table, 100);
	if (!failure && basic_test(&table) 	   != 0) failure = true;
	if (!failure && not_found_test(&table) != 0) failure = true;
	if (!failure && duplicate_test(&table) != 0) failure = true;
	//if (!failure && remove_test(&table)    != 0) failure = true; <---- Causes segfault
	if (!failure && resize_test(&table)	   != 0)
	    failure = true;
	if (!failure && set_test(&table)       != 0) failure = true;
    print_lengths(&table);
	free_hashtable(&table, true, NULL);
	if (failure) {
        printf("Failure on hashtable!\n");
	    return -1;
    }
	return 0;
}