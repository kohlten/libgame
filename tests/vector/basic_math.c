//
// Created by Alexander Strole on 12/1/18.
//

#include "basic_math.h"
#include "vector.h"
#include "assert.h"

void test_basic_math_vector2f()
{
    t_vector2f one;
    t_vector2f two;
    t_vector2f out;

    one.x = 10;
    one.y = 10;
    two = one;
    out = add_vector2f(one, two);
    assert(out.x == 20 && out.y == 20, "Basic math failed!\n");
    out = sub_vector2f(one, two);
    assert(out.x == 0 && out.y == 0, "Basic math failed!\n");
    out = mul_vector2f(one, two);
    assert(out.x == 100 && out.y == 100, "Basic math failed!\n");
    out = div_vector2f(one, two);
    assert(out.x == 1 && out.y == 1, "Basic math failed!\n");
}

void test_basic_math_vector2i()
{
    t_vector2i one;
    t_vector2i two;
    t_vector2i out;

    one.x = 10;
    one.y = 10;
    two = one;
    out = add_vector2i(one, two);
    assert(out.x == 20 && out.y == 20, "Basic math failed!\n");
    out = sub_vector2i(one, two);
    assert(out.x == 0 && out.y == 0, "Basic math failed!\n");
    out = mul_vector2i(one, two);
    assert(out.x == 100 && out.y == 100, "Basic math failed!\n");
    out = div_vector2i(one, two);
    assert(out.x == 1 && out.y == 1, "Basic math failed!\n");
}

void test_basic_math_vector2u()
{
    t_vector2u one;
    t_vector2u two;
    t_vector2u out;

    one.x = 10;
    one.y = 10;
    two = one;
    out = add_vector2u(one, two);
    assert(out.x == 20 && out.y == 20, "Basic math failed!\n");
    out = sub_vector2u(one, two);
    assert(out.x == 0 && out.y == 0, "Basic math failed!\n");
    out = mul_vector2u(one, two);
    assert(out.x == 100 && out.y == 100, "Basic math failed!\n");
    out = div_vector2u(one, two);
    assert(out.x == 1 && out.y == 1, "Basic math failed!\n");
}

void test_basic_math_vector2l()
{
    t_vector2l one;
    t_vector2l two;
    t_vector2l out;

    one.x = 10;
    one.y = 10;
    two = one;
    out = add_vector2l(one, two);
    assert(out.x == 20 && out.y == 20, "Basic math failed!\n");
    out = sub_vector2l(one, two);
    assert(out.x == 0 && out.y == 0, "Basic math failed!\n");
    out = mul_vector2l(one, two);
    assert(out.x == 100 && out.y == 100, "Basic math failed!\n");
    out = div_vector2l(one, two);
    assert(out.x == 1 && out.y == 1, "Basic math failed!\n");
}

void test_basic_math_vector2ul()
{
    t_vector2ul one;
    t_vector2ul two;
    t_vector2ul out;

    one.x = 10;
    one.y = 10;
    two = one;
    out = add_vector2ul(one, two);
    assert(out.x == 20 && out.y == 20, "Basic math failed!\n");
    out = sub_vector2ul(one, two);
    assert(out.x == 0 && out.y == 0, "Basic math failed!\n");
    out = mul_vector2ul(one, two);
    assert(out.x == 100 && out.y == 100, "Basic math failed!\n");
    out = div_vector2ul(one, two);
    assert(out.x == 1 && out.y == 1, "Basic math failed!\n");
}