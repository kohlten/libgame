//
// Created by Alexander Strole on 12/2/18.
//

#include "op_by.h"
#include "vector.h"
#include "assert.h"

void test_mul_by()
{
    t_vector2f f;
    t_vector2i i;
    t_vector2u u;
    t_vector2l l;
    t_vector2ul ul;

    f = mul_by_vector2f(((t_vector2f){10, 10}), 2);
    assert(f.x == 20 && f.y == 20, "Mul by failed!\n");
    i = mul_by_vector2i(((t_vector2i){10, 10}), 2);
    assert(i.x == 20 && i.y == 20, "Mul by failed!\n");
    u = mul_by_vector2u(((t_vector2u){10, 10}), 2);
    assert(u.x == 20 && u.y == 20, "Mul by failed!\n");
    l = mul_by_vector2l(((t_vector2l){10, 10}), 2);
    assert(l.x == 20 && l.y == 20, "Mul by failed!\n");
    ul = mul_by_vector2ul(((t_vector2ul){10, 10}), 2);
    assert(ul.x == 20 && ul.y == 20, "Mul by failed!\n");
}

void test_div_by()
{
    t_vector2f f;
    t_vector2i i;
    t_vector2u u;
    t_vector2l l;
    t_vector2ul ul;

    f = div_by_vector2f(((t_vector2f){10, 10}), 2);
    assert(f.x == 5 && f.y == 5, "Div by failed!\n");
    i = div_by_vector2i(((t_vector2i){10, 10}), 2);
    assert(i.x == 5 && i.y == 5, "Div by failed!\n");
    u = div_by_vector2u(((t_vector2u){10, 10}), 2);
    assert(u.x == 5 && u.y == 5, "Div by failed!\n");
    l = div_by_vector2l(((t_vector2l){10, 10}), 2);
    assert(l.x == 5 && l.y == 5, "Div by failed!\n");
    ul = div_by_vector2ul(((t_vector2ul){10, 10}), 2);
    assert(ul.x == 5 && ul.y == 5, "Div by failed!\n");
}