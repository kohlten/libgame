//
// Created by Alexander Strole on 12/1/18.
//ß

#include "vector.h"
#include "basic_math.h"
#include "op_by.h"

int main()
{
    test_basic_math_vector2f();
    test_basic_math_vector2i();
    test_basic_math_vector2l();
    test_basic_math_vector2u();
    test_basic_math_vector2ul();
    test_div_by();
    test_mul_by();
}