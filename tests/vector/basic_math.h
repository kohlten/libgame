//
// Created by Alexander Strole on 12/1/18.
//

#ifndef BASIC_MATH_H
#define BASIC_MATH_H

void test_basic_math_vector2f();
void test_basic_math_vector2i();
void test_basic_math_vector2l();
void test_basic_math_vector2u();
void test_basic_math_vector2ul();

#endif
