//
// Created by Alexander Strole on 12/5/18.
//

#include "data_structures/vector_array.h"
#include "assert.h"
#include <stdlib.h>
#include "libft.h"
#include <stdio.h>
#include <string.h>

void test_basic_append()
{
    t_dynamic_array *array;
    char **datas;
    char *data;
    char *out;
    int value;
    int items;

    items = 500;
    array = new_dynamic_array();
    assert(array != NULL, "Failed to malloc dynamic array!\n");
    datas = ft_memalloc(sizeof(char *) * items);
    for (int i = 0; i < items; i++) {
        data = ft_memalloc(5 * sizeof(char));
        strcpy(data, "1234\0");
        datas[i] = data;
        value = append_to_array(array, data);
        assert(value == 0, "Failed to append data!\n");
        assert(array->data[i] == data, "Failed to append data!\n");
    }
    for (int i = 0; i < items; i++)
    {
        value = get_from_array(array, i, (void **)&data);
        assert(value == 0, "Failed to get from array!\n");
        assert(data == datas[i], "Failed to get from array!\n");
        assert(strcmp(data, "1234\0") == 0, "Failed to get from array!\n");
        free(data);
    }
    free(datas);
    free_dynamic_array(array);
}

void test_basic_remove()
{
    t_dynamic_array *array;
    char *data;
    int value;

    array = new_dynamic_array();
    assert(array != NULL, "Failed to malloc dynamic array!\n");
    data = ft_memalloc(5 * sizeof(char));
    assert(data != NULL, "Failed to malloc data!\n");
    strcpy(data, "data\0");
    append_to_array(array, data);
    append_to_array(array, data);
    value = remove_from_array(array, 0, 1, NULL);
    assert(value == 0, "Failed to get from array!\n");
    free_dynamic_array(array);
}

void test_hard_remove()
{
    t_dynamic_array *array;
    char **datas;
    char *data;
    char *out;
    int value;
    int items;

    items = 50000;
    array = new_dynamic_array();
    assert(array != NULL, "Failed to malloc dynamic array!\n");
    datas = ft_memalloc(sizeof(char *) * items);
    for (int i = 0; i < items; i++) {
        data = ft_memalloc(5 * sizeof(char));
        strcpy(data, "1234\0");
        datas[i] = data;
        value = append_to_array(array, data);
        assert(value == 0, "Failed to append data!\n");
        assert(array->data[i] == data, "Failed to append data!\n");
    }
    for (int i = 0; i < items; i++) {

    }
}

int main()
{
    test_basic_append();
    test_basic_remove();
}
