//
// Created by kohlten on 1/1/19.
//

#include "rendering/animator.h"
#include "texture.h"
#include "window.h"
#include "fps.h"
#include "libft.h"
#include "game_io.h"
#include <stdio.h>

char **load_files(char *filename) {
    char *file_text;
    char **split;

    file_text = read_file(filename);
    if (!file_text)
        return (NULL);
    split = ft_strsplit(file_text, '\n');
    free(file_text);
    if (!split)
        return (NULL);
    return split;
}

t_window *init() {
    t_window *window;

    if (SDL_Init(SDL_INIT_VIDEO) < 0)
    {
        printf("SDL cout_ld not initialize! SDL_Error: %s\n", SDL_GetError());
        return NULL;
    }
    window = new_window((SDL_Rect) {SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 1080, 720}, "Animation test", 0, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
    if (!window)
        return NULL;
    return (window);
}

int init_animation(t_animator *animator, SDL_Renderer *renderer) {
    vector vec;
    char **files;

    if (init_animator(animator) != 0)
        return (-1);
    vector_init(&vec);
    files = load_files("../files");
    if (!files)
        return (-2);
    if (load_textures(&vec, renderer, files, 179) != 0)
        return (-3);
    for (int i = 0; i < 180; i++)
        free(files[i]);
    free(files);
    if (add_state_animator(animator, "default", &vec, 1, 0, -1) != 0)
        return (-4);
    vector_free(&vec);
    if (change_state_animator(animator, "default") != 0)
        return (-5);
    if (start_playing_animator(animator) != 0)
        return (-6);
    return 0;
}

int main() {
    t_window *window;
    t_animator animator;
    int done = 0;
    int frames = 0;
    SDL_Event e;

    window = init();
    if (!window)
        return (-1);
    if (init_animation(&animator, window->SDLrenderer) != 0)
        return -1;
    while (!done && frames < 181) {
        while (SDL_PollEvent(&e)) {
            if (e.type == SDL_QUIT)
                done = 1;
            if (e.type == SDL_KEYDOWN && e.key.keysym.sym == SDLK_ESCAPE)
                done = 1;
        }
        if (update_animator(&animator) != 0)
            return -1;
        SDL_SetRenderDrawColor(window->SDLrenderer, 0, 0, 0, 255);
        SDL_RenderClear(window->SDLrenderer);
        SDL_SetRenderDrawColor(window->SDLrenderer, 255, 255, 255, 255);
        if (draw_animator(&animator, window->SDLrenderer, (t_vector2i){0, 0}, 0, NULL, (t_vector2i){0, 0}, SDL_FLIP_NONE) != 0)
            return -1;
        SDL_RenderPresent(window->SDLrenderer);
        frames++;
    }
    free_animator(&animator);
    free_window(window);
    return (0);
}


