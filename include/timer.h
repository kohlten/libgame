//
// Created by kohlten on 1/4/19.
//

#ifndef TIMER_H
#define TIMER_H

#include <stdbool.h>
#include <SDL.h>
#include <stdint.h>

typedef struct s_timer {
    Uint32 start_time;
    Uint32 target_time;
    bool started;
    bool inited;
}               t_timer;

void init_timer(t_timer *timer, uint32_t target_time);
void start_timer(t_timer *timer);
void stop_timer(t_timer *timer);
Uint32 get_time_timer(t_timer *timer);
bool timer_done(t_timer *timer);
void update_timer(t_timer *timer);
void reset_timer(t_timer *timer);
bool timer_running(t_timer *timer);

void wait_time(Uint32 time);

#endif //TIMER_H
