//
// Created by Alexander Strole on 12/3/18.
//

#ifndef STACK_H
#define STACK_H

#include "data_structures/linked_list.h"

typedef struct  s_stack
{
    t_linked_list *list;
    int len;
}               t_stack;

t_stack *new_stack();
void *pop_stack(t_stack *stack);
int push_stack(t_stack *stack, void *elem);
int get_len_stack(t_stack *stack);
void free_stack(t_stack *stack, int free_info, void (*free_func)(void *data));

#endif //STACK_H
