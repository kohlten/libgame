#ifndef GAME_IO_H
#define GAME_IO_H

char *read_file(char *name);
char *get_path_of_file(char *path);

#endif