//
// Created by Alexander Strole on 12/2/18.
//

#ifndef PHYSICS_OBJECT_H
#define PHYSICS_OBJECT_H

#include "vector/vector.h"

typedef struct s_physics_object
{
    t_vector2f pos;
    t_vector2f vel;
    t_vector2f acc;
}               t_physics_object;

void init_physics_object(t_physics_object *object);
void apply_force(t_physics_object *object, t_vector2f force);
void change_pos_object(t_physics_object *object, t_vector2f new_pos);
void change_vel_object(t_physics_object *object, t_vector2f new_pos);
void update_object(t_physics_object *object);

#endif
