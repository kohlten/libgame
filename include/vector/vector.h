#ifndef VECTOR_H
#define VECTOR_H
#include "math.h"
#include "random.h"
#include <sodium.h>
#include <stdbool.h>

// All functions require that the angle is in radians

typedef struct s_vector2f
{
	double x;
	double y;
} t_vector2f;

typedef struct s_vector2i
{
	signed int x;
	signed int y;
} t_vector2i;

typedef struct s_vector2u
{
	unsigned int x;
	unsigned int y;
}	t_vector2u;

typedef struct s_vector2l
{
	signed long x;
	signed long y;
} t_vector2l;

typedef struct s_vector2ul
{
	unsigned long x;
	unsigned long y;
}	t_vector2ul;

// Constructors
# define new_vector2f(x, y) (t_vector2f) {x, y}
# define new_vector2i(x, y) (t_vector2i) {x, y}
# define new_vector2u(x, y) (t_vector2u) {x, y}
# define new_vector2l(x, y) (t_vector2l) {x, y}
# define new_vector2ul(x, y) (t_vector2ul) {x, y}

// Basic math functions
# define dot_vector(one, two) (one.x * two.x) + (one.y * two.y)
# define cross_vector(one, two) one.x * two.x - one.y * two.y
# define angle_between_vectors(one, two) atan2(cross_vector(one, two), dot_vector(one, two))
# define magsqrt_vector(vec) (vec.x * vec.x) + (vec.y * vec.y)
# define mag_vector(vec) sqrt(magsqrt_vector(vec))
# define dist_vector(one, two) sqrt(pow(one.x - two.x, 2) + pow(one.y - two.y, 2))
# define heading_vector(vec) atan2(vec.y, vec.x)

// Zero definitions
# define vector2f_zero (t_vector2f) {0, 0}
# define vector2i_zero (t_vector2i) {0, 0}
# define vector2u_zero (t_vector2u) {0, 0}
# define vector2l_zero (t_vector2l) {0, 0}
# define vector2ul_zero (t_vector2ul) {0, 0}

// Basic math
# define add_vector2f(one, two) (t_vector2f) {one.x + two.x, one.y + two.y}
# define sub_vector2f(one, two) (t_vector2f) {one.x - two.x, one.y - two.y}
# define mul_vector2f(one, two) (t_vector2f) {one.x * two.x, one.y * two.y}
# define div_vector2f(one, two) (t_vector2f) {one.x / two.x, one.y / two.y}

# define add_vector2i(one, two) (t_vector2i) {one.x + two.x, one.y + two.y}
# define sub_vector2i(one, two) (t_vector2i) {one.x - two.x, one.y - two.y}
# define mul_vector2i(one, two) (t_vector2i) {one.x * two.x, one.y * two.y}
# define div_vector2i(one, two) (t_vector2i) {one.x / two.x, one.y / two.y}

# define add_vector2u(one, two) (t_vector2u) {one.x + two.x, one.y + two.y}
# define sub_vector2u(one, two) (t_vector2u) {one.x - two.x, one.y - two.y}
# define mul_vector2u(one, two) (t_vector2u) {one.x * two.x, one.y * two.y}
# define div_vector2u(one, two) (t_vector2u) {one.x / two.x, one.y / two.y}

# define add_vector2l(one, two) (t_vector2l) {one.x + two.x, one.y + two.y}
# define sub_vector2l(one, two) (t_vector2l) {one.x - two.x, one.y - two.y}
# define mul_vector2l(one, two) (t_vector2l) {one.x * two.x, one.y * two.y}
# define div_vector2l(one, two) (t_vector2l) {one.x / two.x, one.y / two.y}

# define add_vector2ul(one, two) (t_vector2ul) {one.x + two.x, one.y + two.y}
# define sub_vector2ul(one, two) (t_vector2ul) {one.x - two.x, one.y - two.y}
# define mul_vector2ul(one, two) (t_vector2ul) {one.x * two.x, one.y * two.y}
# define div_vector2ul(one, two) (t_vector2ul) {one.x / two.x, one.y / two.y}

// Multiply by n
# define mul_by_vector2f(vec, n) (t_vector2f) {vec.x * n, vec.y * n}
# define mul_by_vector2i(vec, n) (t_vector2i) {vec.x * n, vec.y * n}
# define mul_by_vector2u(vec, n) (t_vector2u) {vec.x * n, vec.y * n}
# define mul_by_vector2l(vec, n) (t_vector2l) {vec.x * n, vec.y * n}
# define mul_by_vector2ul(vec, n) (t_vector2ul) {vec.x * n, vec.y * n}

// Divide by n
# define div_by_vector2f(vec, n) (t_vector2f) {vec.x / n, vec.y / n}
# define div_by_vector2i(vec, n) (t_vector2i) {vec.x / n, vec.y / n}
# define div_by_vector2u(vec, n) (t_vector2u) {vec.x / n, vec.y / n}
# define div_by_vector2l(vec, n) (t_vector2l) {vec.x / n, vec.y / n}
# define div_by_vector2ul(vec, n) (t_vector2ul) {vec.x / n, vec.y / n}

// Set magnitude
# define setmag_vector2f(vec, n) mul_by_vector2f(norm_vector2f(vec), n)
# define setmag_vector2i(vec, n) mul_by_vector2i(norm_vector2i(vec), n)
# define setmag_vector2u(vec, n) mul_by_vector2u(norm_vector2u(vec), n)
# define setmag_vector2l(vec, n) mul_by_vector2l(norm_vector2i(vec), n)
# define setmag_vector2ul(vec, n) mul_by_vector2ul(norm_vector2ul(vec), n)

// Casting
# define vector2i_to_vector2f(vec) new_vector2f((double)vec.x, (double)vec.y)
# define vector2u_to_vector2f(vec) new_vector2f((double)vec.x, (double)vec.y)
# define vector2l_to_vector2f(vec) new_vector2f((double)vec.x, (double)vec.y)
# define vector2ul_to_vector2f(vec) new_vector2f((double)vec.x, (double)vec.y)

# define vector2f_to_vector2i(vec) new_vector2i((signed int)vec.x, (signed int)vec.y)
# define vector2u_to_vector2i(vec) new_vector2i((signed int)vec.x, (signed int)vec.y)
# define vector2l_to_vector2i(vec) new_vector2i((signed int)vec.x, (signed int)vec.y)
# define vector2ul_to_vector2i(vec) new_vector2i((signed int)vec.x, (signed int)vec.y)

# define vector2i_to_vector2u(vec) new_vector2u((unsigned int)vec.x, (unsigned int)vec.y)
# define vector2f_to_vector2u(vec) new_vector2u((unsigned int)vec.x, (unsigned int)vec.y)
# define vector2l_to_vector2u(vec) new_vector2u((unsigned int)vec.x, (unsigned int)vec.y)
# define vector2ul_to_vector2u(vec) new_vector2u((unsigned int)vec.x, (unsigned int)vec.y)

# define vector2i_to_vector2l(vec) new_vector2l((signed long)vec.x, (signed long)vec.y)
# define vector2f_to_vector2l(vec) new_vector2l((signed long)vec.x, (signed long)vec.y)
# define vector2u_to_vector2l(vec) new_vector2l((signed long)vec.x, (signed long)vec.y)
# define vector2ul_to_vector2l(vec) new_vector2l((signed long)vec.x, (signed long)vec.y)

# define vector2i_to_vector2ul(vec) new_vector2ul((unsigned long)vec.x, (unsigned long)vec.y)
# define vector2f_to_vector2ul(vec) new_vector2ul((unsigned long)vec.x, (unsigned long)vec.y)
# define vector2u_to_vector2ul(vec) new_vector2ul((unsigned long)vec.x, (unsigned long)vec.y)
# define vector2l_to_vector2ul(vec) new_vector2ul((unsigned long)vec.x, (unsigned long)vec.y)

// Create vector from angle
# define vector2f_from_angle(angle, length) new_vector2f(length * cos(angle), length * sin(angle))
# define vector2i_from_angle(angle, length) new_vector2i(length * cos(angle), length * sin(angle))
# define vector2u_from_angle(angle, length) new_vector2u(length * cos(angle), length * sin(angle))
# define vector2l_from_angle(angle, length) new_vector2l(length * cos(angle), length * sin(angle))
# define vector2ul_from_angle(angle, length) new_vector2ul(length * cos(angle), length * sin(angle))

// Create new random vector
# define random_vector2f() vector2f_from_angle(random_double() * (M_PI * 2), 1)

// Normalize
t_vector2f norm_vector2f(t_vector2f vec);
t_vector2i norm_vector2i(t_vector2i vec);
t_vector2u norm_vector2u(t_vector2u vec);
t_vector2l norm_vector2l(t_vector2l vec);
t_vector2ul norm_vector2ul(t_vector2ul vec);

// Rotate
t_vector2f rotate_vector2f(t_vector2f vec, t_vector2f origin, double angle);
t_vector2i rotate_vector2i(t_vector2i vec, t_vector2i origin, double angle);
t_vector2u rotate_vector2u(t_vector2u vec, t_vector2u origin, double angle);
t_vector2l rotate_vector2l(t_vector2l vec, t_vector2l origin, double angle);
t_vector2ul rotate_vector2ul(t_vector2ul vec, t_vector2ul origin, double angle);

// Limit
t_vector2f limit_vector2f(t_vector2f vec, double limit);
t_vector2i limit_vector2i(t_vector2i vec, double limit);
t_vector2u limit_vector2u(t_vector2u vec, double limit);
t_vector2l limit_vector2l(t_vector2l vec, double limit);
t_vector2ul limit_vector2ul(t_vector2ul vec, double limit);

// Map
double	constrain(double n, double low, double high);
double	map(double n, t_vector2f range1, t_vector2f range2);
double	map_constrain(double n, t_vector2f range1, t_vector2f range2);

// Ray Casting
bool ray_intersects_pos(t_vector2f pos, t_vector2f dir, t_vector2f point, double dist, t_vector2f *instersect_point);
#endif