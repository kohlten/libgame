//
// Created by kohlten on 1/3/19.
//

#ifndef COLLISIONS_H
#define COLLISIONS_H

#include <stdbool.h>
#include "vector/vector.h"

bool point_in_circle(t_vector2f point, t_vector2f circle_pos, double circle_radius);
bool line_in_circle(t_vector2f line_start, t_vector2f line_end, t_vector2f circle_center, double circle_radius);
bool triangle_in_circle(t_vector2f p1, t_vector2f p2, t_vector2f p3, t_vector2f circle_center, double circle_radius);

#endif //COLLISIONS_H
