//
// Created by Alexander Strole on 12/2/18.
//

#ifndef RANDOM_H
#define RANDOM_H

double random_double();
int randint(int low, int upper);

#endif
