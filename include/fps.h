#ifndef FPS_H
#define FPS_H

#include <stdbool.h>

typedef struct s_fps {
    int previous_time;
    int frames;
    int current_fps;
    unsigned int start_time;
    int ticks_per_frame;
    bool inited;
}               t_fps;

void init_fps(t_fps *fps, int target_fps);
void update_fps(t_fps *fps);
int get_fps(t_fps *fps);
void limit_fps(t_fps *fps);

#endif