#ifndef GLYPH_TABLE_H
#define GLYPH_TABLE_H

#include "vector/vector.h"
#include "data_structures/hashtable.h"

typedef enum
{
	GLYPH_SUCCESS         =  0,
	GLYPH_HASHTABLE_ERROR = -1,
	GLYPH_MALLOC_ERROR   = -2,
	GLYPH_FILE_ERROR      = -3, 
	GLYPH_TEXTURE_ERROR   = -4,
	GLYPH_VALUE_ERROR     = -5,
	GLYPH_JSON_ERROR	  = -6,
} glyph_table_error;

typedef struct s_glyph_node
{
	t_vector2u	size;
	t_vector2u	pos;
	t_vector2u	offset;
	int			xadvance;
}	t_glyph_node;

typedef struct s_glyph_table
{
	t_hashtable hashtable;
	char		*image_file_name;
	int			spacing;
	int			font_size;
}	t_glyph_table;

glyph_table_error	init_glyph_table		(t_glyph_table *table, long size);
glyph_table_error	set_glyph_table_values	(t_glyph_table *table, char *filename);
glyph_table_error	get_glyph_table_node	(t_glyph_table *table, char character, t_glyph_node **node);
glyph_table_error	remove_glyph_table_node	(t_glyph_table *table, char character, void (*free_func)(void *ptr));
void 				free_glyph_table		(t_glyph_table *table, void (*free_func)(void *ptr));

#endif