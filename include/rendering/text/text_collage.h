//
// Created by kohlten on 1/14/19.
//

#ifndef TEXT_COLLAGE_H
#define TEXT_COLLAGE_H

#include <SDL.h>

#include "data_structures/hashtable.h"
#include "vector/vector.h"
#include "surface.h"
#include "stdbool.h"

typedef struct s_text_collage {
    t_hashtable characters;
    t_vector2i size;
    bool inited;
}               t_text_collage;

int init_text_collage(t_text_collage *collage, t_vector2i size, char *file, char *order, SDL_PixelFormat *format);
int init_text_collage_json(t_text_collage *collage, char *json_file, SDL_PixelFormat *format);
int draw_text_collage(t_text_collage *collage, char *text, SDL_Renderer *renderer, t_vector2i pos, double angle, double zoom, bool smooth, t_vector2i *offsets, int num_offsets);
t_vector2i get_text_size_text_collage(t_text_collage *collage, char *text, double zoom);
void free_text_collage(t_text_collage *collage);

#endif //TEXT_COLLAGE_H
