#ifndef TEXT_FONT_H
#define TEXT_FONT_H

#include <SDL.h>

#include "surface.h"
#include "vector/vector.h"

typedef struct s_text_font {
    TTF_Font *font;
    SDL_Texture *texture;
    int font_size;
    t_vector2i size;
    t_vector2i position;
    SDL_Color color;
} t_text_font;

SDL_Texture *load_from_rendered_text(SDL_Renderer *renderer, char *text, TTF_Font *font, SDL_Color color);
TTF_Font *load_font(char *name, int size);
t_text_font *new_text_font(char *string, SDL_Renderer *renderer, TTF_Font *font, SDL_Color color, t_vector2i position);
t_text_font *change_text_string_font(t_text_font *text, SDL_Renderer *renderer, char *new);
void render_text_font(t_text_font *text, SDL_Renderer *renderer);
void free_text_font(t_text_font *text);

#endif