#ifndef FONT_H
#define FONT_H

#include "rendering/text/text_font.h"
#include "rendering/text/text_bitmap.h"

enum
{
	TYPE_FONT,
	TYPE_BITMAP,
};

typedef struct s_text
{
	t_text_font		*text_font;
	t_text_bitmap	*text_bitmap;
	int				type;
} t_text;

t_text 		*new_text(char *string, SDL_Renderer *renderer, SDL_Color color,
				t_vector2i position, char *bitmap_json_name, TTF_Font *font);
t_text 		*change_text_string(t_text *text, SDL_Renderer *renderer, char *new);
void		render_text(t_text *text, SDL_Renderer *renderer);
void		free_text(t_text *text);
#endif