#ifndef TEXT_BITMAP_H
#define TEXT_BITMAP_H

#include <SDL.h>

#include "texture.h"
#include "surface.h"
#include "vector/vector.h"
#include "rendering/text/glyph_table.h"

typedef struct s_text_bitmap
{
	t_texture		*bitmap;
	t_texture		*texture;
	char			*string;
	t_glyph_table	glyph_table;
	bool			string_changed;
	t_vector2i		size;
	t_vector2i		position;
	SDL_Color		color;
} t_text_bitmap;

t_text_bitmap 	*new_text_bitmap(char *string, SDL_Renderer *renderer, char *bitmap_json_name, SDL_Color color, t_vector2i position);
t_text_bitmap	*change_text_string_bitmap(t_text_bitmap *text, char *new);
void 			render_text_bitmap(t_text_bitmap *text, SDL_Renderer *renderer);
void			free_text_bitmap(t_text_bitmap *text);

#endif