//
// Created by kohlten on 12/16/18.
//

#ifndef ANIMATOR_H
#define ANIMATOR_H

#include "data_structures/vector_array.h"
#include "data_structures/hashtable.h"
#include "vector/vector.h"
#include <SDL.h>
#include "surface.h"

typedef struct s_animator
{
    t_hashtable states;
    char *current_state;
    int total_frames;
    int inited;
}               t_animator;

typedef struct s_animator_state_node
{
    vector images;
    int current_img;
    int wait_time;
    int playing;
    int times_played;
    int max_times_played;
    int frame_start_time;
}               t_animator_state_node;

int init_animator(t_animator *animator);
int add_state_animator(t_animator *animator, char *state_name, vector *textures,
        int wait_time, int current_img, int max_times_played);
int update_animator(t_animator *animator);
int draw_animator(t_animator *animator, SDL_Renderer *renderer, t_vector2i pos, double angle, double zoom, bool smooth);
int change_state_animator(t_animator *animator, char *state_name);
int start_playing_animator(t_animator *animator);
int stop_playing_animator(t_animator *animator);
int get_frame_animator(t_animator *animator);
int get_num_frames_animator(t_animator *animator);
int set_frame_animator(t_animator *animator, int image, char *state_name);
t_vector2i get_frame_size(t_animator *animator);
bool is_playing_animator(t_animator *animator);
char *get_current_state_animator(t_animator *animator);
t_surface *get_current_image_animator(t_animator *animator);
void free_animator(t_animator *animator, bool free_textures);

#endif //ANIMATOR_H
