//
// Created by kohlten on 1/5/19.
//

#ifndef BASIC_SHAPES_H
#define BASIC_SHAPES_H

#include "vector/vector.h"
#include <SDL.h>
#include <stdbool.h>

void draw_circle(t_vector2i center, int size, bool filled, SDL_Color color, SDL_Renderer *renderer);
void draw_rect(SDL_Renderer *renderer, t_vector2i pos, t_vector2i size, bool aa, short r, short g, short b, short a);

#endif //BASIC_SHAPES_H
