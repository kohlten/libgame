//
// Created by Alexander Strole on 12/11/18.
//

#ifndef GUI_IMAGE_H
#define GUI_IMAGE_H

#include "data_structures/vector_array.h"
#include "vector/vector.h"
#include <SDL.h>

typedef struct s_gui_image
{
    t_vector2i position;
    t_vector2i size;
    vector sprites;
    int current;
    double animation_speed;
} t_gui_image;

t_gui_image *new_gui_image();
void draw_gui_image(t_gui_image *image, SDL_Renderer *renderer);
void free_gui_image(t_gui_image *image);

#endif //GUI_IMAGE_H
