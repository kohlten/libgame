//
// Created by kohlten on 1/15/19.
//

#ifndef GUI_BUTTON_H
#define GUI_BUTTON_H

#include <SDL.h>
#include <stdbool.h>
#include "vector/vector.h"
#include "rendering/text/text_collage.h"

typedef struct s_gui_button {
    t_surface *hovering_surf;
    t_surface *clicked_surf;
    t_surface *normal_surf;
    t_text_collage *text;
    t_vector2i pos;
    t_vector2i size;
    char *string;
    bool hovering;
    bool clicked;
}               t_gui_button;

int init_gui_button(t_gui_button *button, t_text_collage *text, t_vector2i pos, t_vector2i button_size,
                    t_surface *regular, t_surface *hover, t_surface *pressed);
void update_gui_button(t_gui_button *button, t_vector2i mouse_pos, bool mouse_clicked);
void draw_gui_button(t_gui_button *button, SDL_Renderer *renderer, double angle, double zoom, bool aa, bool debug);
int change_string_gui_button(t_gui_button *button, char *string);
void reset_gui_button(t_gui_button *button);
void free_gui_button(t_gui_button *button);

#endif //GUI_BUTTON_H
