//
// Created by Alexander Strole on 12/11/18.
//

#ifndef GUI_H
#define GUI_H

#include "data_structures/hashtable.h"
#include "data_structures/vector_array.h"
#include "surface.h"

typedef enum
{
    GUI_IMAGE = 0,
    GUI_TEXT = 1,
}   gui_type;

typedef enum
{
    GUI_HASHTABLE_ERROR = -5,
    GUI_MALLOC_ERROR = -4,
    GUI_UNKNOWN_TYPE = -3,
    GUI_JSON_ERROR = -2,
    GUI_FILE_ERROR = -1,
    GUI_SUCCESS = 0,
}   gui_error;

typedef struct s_gui
{
    vector objects;
    t_hashtable fonts;
    t_hashtable images;
    int inited;
}  t_gui;

typedef struct s_gui_node
{
    gui_type type;
    void *node;
} t_gui_node;


gui_error init_gui(t_gui *gui, char *filename, SDL_Renderer *renderer);
void display_gui(t_gui *gui, SDL_Renderer *renderer);
#endif //GUI_H
