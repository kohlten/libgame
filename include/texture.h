//
// Created by Alexander Strole on 12/19/18.
//

#ifndef TEXTURE_H
#define TEXTURE_H

#include "surface.h"
#include "data_structures/vector_array.h"

typedef struct s_texture {
    SDL_Texture *texture;
    int width;
    int height;
} t_texture;

t_texture *new_texture(SDL_Renderer *renderer, char *filename);
void set_color_texture(t_texture *texture, SDL_Color color);
void set_blend_texture(t_texture *texture, SDL_BlendMode mode);
void render_texture(t_texture *texture, SDL_Renderer *renderer, t_vector2i pos, SDL_Rect *clip, double angle,
                    t_vector2i center, SDL_RendererFlip flip);
int load_textures(vector *tex_arr, SDL_Renderer *renderer, char **filenames, int files);
int resize_texture(t_texture *texture, SDL_Renderer *renderer, t_vector2i new_size);
void set_texture(t_texture *texture, SDL_Texture *sdltexture);
int load_new_texture(t_texture *texture, char *filename, SDL_Renderer *renderer);
void free_texture(t_texture *texture);

#endif //TEXTURE_H
