#ifndef SURFACE_H
#define SURFACE_H

#include <SDL_image.h>
#include <SDL_ttf.h>
#include <stdbool.h>

#include "vector/vector.h"

typedef struct s_surface {
    SDL_Surface *surface;
    t_vector2i size;
    SDL_PixelFormat *format;
}               t_surface;

t_surface *new_surface(const char *name);
t_surface *new_unset_surface();
t_surface *new_from_surface(SDL_Surface *sdlsurface);
t_surface *new_blank_surface(t_vector2i size, SDL_PixelFormat *format);
int resize_surface(t_surface *surface, t_vector2i new_size, t_vector2i pos);
void wipe_surface(t_surface *surface, int color);
uint32_t get_pixel(t_surface *surface, t_vector2i pos);
void set_pixel(t_surface *surface, t_vector2i pos, uint32_t pixel);
int	draw_surface(t_surface *surface, SDL_Renderer *renderer, t_vector2i pos);
t_surface *rotate_surface(t_surface *surface, double angle, double zoom, bool smooth);
int rotate_and_draw_surface(t_surface *surface, SDL_Renderer *renderer, t_vector2i pos, double angle, double zoom, bool smooth);
void free_surface(void *p);
#endif