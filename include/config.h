//
// Created by Alexander Strole on 12/7/18.
//

#ifndef CONFIG_H
#define CONFIG_H

#include "data_structures/hashtable.h"
#include <SDL.h>
#include "vector/vector.h"
#include <stdbool.h>

typedef enum
{
    CONFIG_NUM      = 0,
    CONFIG_BOOL     = 1,
    CONFIG_VECTOR2  = 2,
    CONFIG_COLOR    = 3,
} t_node_type;

typedef enum
{
    CONFIG_MALLOC_ERROR   = -1,
    CONFIG_VALUE_ERROR    = -2,
    CONFIG_HASTABLE_ERROR = -3,
    CONFIG_FILE_ERROR     = -4,
    CONFIG_SUCCESS        =  0,
} t_config_error;

typedef struct s_config
{
    t_hashtable table;
    char *filename;
    int initialized;
}              t_config;

typedef struct s_config_node
{
    t_node_type type;
    void *info;
}              t_config_node;

t_config_error init_config(t_config *config, char *filename, int hash_len);
t_config_error get_value_config_num(t_config *config, char *key, float *b);
t_config_error get_value_config_bool(t_config *config, char *key, bool *b);
t_config_error get_value_config_vector(t_config *config, char *key, t_vector2f *vec);
t_config_error get_value_config_color(t_config *config, char *key, SDL_Color *color);
void free_config(t_config *config);
#endif //CONFIG_H
