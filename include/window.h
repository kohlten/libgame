#ifndef WINDOW_H
#define WINDOW_H

#include <SDL.h>
#include "config.h"

typedef struct s_window
{
	int width;
	int height;
	SDL_Window *SDLwindow;
	SDL_Renderer *SDLrenderer;
} t_window;

t_window *new_window(SDL_Rect window_info, char *name, int window_flags, int renderer_flags);
int get_window_flags(t_config *window_config);
int get_renderer_flags(t_config *window_config);
void setup_window(t_window **window, int *fps, t_config *window_config);
void free_window(t_window *window);

#endif