#include "window.h"
#include "config.h"
#include "assert.h"
#include "libft.h"

#include <GL/glew.h>

#include <stdio.h>

t_window *new_window(SDL_Rect window_info, char *name, int window_flags, int renderer_flags)
{
	t_window *window;
	int buffers;
	int samples;

	window = malloc(sizeof(t_window));
    if (!window)
    {
        // Later replace with logger
        printf("Could not malloc the window!\n");
        return NULL;
    }
    window->SDLwindow = SDL_CreateWindow(name,
        window_info.x, window_info.y, window_info.w, window_info.h, window_flags);
    if (!window->SDLwindow) {
        fprintf(stderr, "SDL could not create a window! SDL_Error: %s\n", SDL_GetError());
        assert(0, "");
    }
    window->SDLrenderer = SDL_CreateRenderer(window->SDLwindow, -1, renderer_flags);
    if (!window->SDLrenderer) {
        fprintf(stderr, "SDL could not create a renderer! SDL Error: %s\n", SDL_GetError());
        assert(0, "");
    }
    SDL_GL_GetAttribute(SDL_GL_MULTISAMPLEBUFFERS, &buffers);
    SDL_GL_GetAttribute(SDL_GL_MULTISAMPLESAMPLES, &samples);
    printf("OpenGL Buffers: %d OpenGL Samples: %d\n", buffers, samples);
    window->width = window_info.w;
    window->height = window_info.h;
    return window;
}

int get_window_flags(t_config *window_config)
{
    int flags;
    bool curr;

    flags = 0;
    if (get_value_config_bool(window_config, "WINDOW_FULLSCREEN", &curr) == CONFIG_SUCCESS && curr) flags |= SDL_WINDOW_FULLSCREEN;
    if (get_value_config_bool(window_config, "WINDOW_SHOWN", &curr) == CONFIG_SUCCESS && curr) flags |= SDL_WINDOW_SHOWN;
    if (get_value_config_bool(window_config, "WINDOW_HIDDEN", &curr) == CONFIG_SUCCESS && curr) flags |= SDL_WINDOW_HIDDEN;
    if (get_value_config_bool(window_config, "WINDOW_BORDERLESS", &curr) == CONFIG_SUCCESS && curr) flags |= SDL_WINDOW_BORDERLESS;
    if (get_value_config_bool(window_config, "WINDOW_RESIZABLE", &curr) == CONFIG_SUCCESS && curr) flags |= SDL_WINDOW_RESIZABLE;
    if (get_value_config_bool(window_config, "WINDOW_MINIMISED", &curr) == CONFIG_SUCCESS && curr) flags |= SDL_WINDOW_MINIMIZED;
    if (get_value_config_bool(window_config, "WINDOW_MAXIMISED", &curr) == CONFIG_SUCCESS && curr) flags |= SDL_WINDOW_MAXIMIZED;
    if (get_value_config_bool(window_config, "WINDOW_ALLOW_HIGHDPI", &curr) == CONFIG_SUCCESS && curr) flags |= SDL_WINDOW_ALLOW_HIGHDPI;
    if (get_value_config_bool(window_config, "WINDOW_ON_TOP", &curr) == CONFIG_SUCCESS && curr) flags |= SDL_WINDOW_ALWAYS_ON_TOP;
    if (get_value_config_bool(window_config, "WINDOW_SKIP_TASKBAR", &curr) == CONFIG_SUCCESS && curr) flags |= SDL_WINDOW_SKIP_TASKBAR;
    if (get_value_config_bool(window_config, "WINDOW_FULLSCREEN_DESKTOP", &curr) == CONFIG_SUCCESS && curr) flags |= SDL_WINDOW_FULLSCREEN_DESKTOP;
    if (get_value_config_bool(window_config, "WINDOW_UTILITY", &curr) == CONFIG_SUCCESS && curr) flags |= SDL_WINDOW_UTILITY;
    if (get_value_config_bool(window_config, "WINDOW_TOOLTIP", &curr) == CONFIG_SUCCESS && curr) flags |= SDL_WINDOW_TOOLTIP;
    if (get_value_config_bool(window_config, "WINDOW_POPUP_MENU", &curr) == CONFIG_SUCCESS && curr) flags |= SDL_WINDOW_POPUP_MENU;
    return flags;
}

int get_renderer_flags(t_config *window_config)
{
    int flags;
    bool curr;

    flags = 0;
    if (get_value_config_bool(window_config, "RENDERER_SOFTWARE", &curr) == CONFIG_SUCCESS && curr) flags |= SDL_RENDERER_SOFTWARE;
    if (get_value_config_bool(window_config, "RENDERER_ACCELERATED", &curr) == CONFIG_SUCCESS && curr) flags |= SDL_RENDERER_ACCELERATED;
    if (get_value_config_bool(window_config, "RENDERER_PRESENTVSYNC", &curr) == CONFIG_SUCCESS && curr) flags |= SDL_RENDERER_PRESENTVSYNC;
    return flags;
}

void setup_window(t_window **window, int *fps, t_config *window_config) {
    SDL_DisplayMode dm;
    t_vector2f size;
    float f_fps;
    float monitor;
    float samples;
    bool multisampling;
    bool vsync;
    SDL_Rect window_rect;
    SDL_Rect temp;

    if (SDL_Init(SDL_INIT_EVERYTHING) != 0) {
        printf("Failed to init SDL. SDL Error: %s\n", SDL_GetError());
        assert(0, "");
    }
    assert(init_config(window_config, "configs/window_options.txt", 15) == CONFIG_SUCCESS,
           "Failed to init window config!\n");
    if (get_value_config_bool(window_config, "TEXTURE_AA", &multisampling) != CONFIG_SUCCESS)
        multisampling = false;
    if (multisampling)
        SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "2");
    if (get_value_config_bool(window_config, "MULTISAMPLING", &multisampling) != CONFIG_SUCCESS)
        multisampling = false;
    if (get_value_config_num(window_config, "MULTISAMPLING_SAMPLES", &samples) != CONFIG_SUCCESS)
        samples = 2;
#ifndef __APPLE__
    if (multisampling) {
        assert(samples == 16 || samples == 8 || samples == 4 || samples == 2, "Samples must be 16, 8, 4, or 2!\n");
        SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
        SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, (int)samples);
        glEnable(GL_MULTISAMPLE);
    }
#endif
    assert(get_value_config_vector(window_config, "WINDOW_SIZE", &size) == CONFIG_SUCCESS,
           "Failed to get window size!\n");
    if (get_value_config_num(window_config, "WINDOW_MONITOR", &monitor) != CONFIG_SUCCESS)
        monitor = 0;
    assert(monitor < SDL_GetNumVideoDisplays(), "Monitor from config is greater than the amount of displays!\n");
    ft_bzero(&window_rect, sizeof(SDL_Rect));
    for (int i = 0; i < monitor; i++) {
        SDL_GetDisplayBounds(i, &temp);
        window_rect.x += temp.w;
    }
    if (SDL_GetCurrentDisplayMode((int) monitor, &dm) != 0) {
        fprintf(stderr, "Failed to create a window on monitor %d\n", (int) monitor);
        assert(0, "");
    }
    assert(size.x <= dm.w && size.y <= dm.h, "Window size is bigger than the display!\n");
    *window = new_window(
            (SDL_Rect) {(int) (window_rect.x + ((dm.w / 2) - (size.x / 2))),
                        (int) (window_rect.y + ((dm.h / 2) - (size.y / 2))), (int) size.x, (int) size.y},
            "asteroids", get_window_flags(window_config), get_renderer_flags(window_config));
    assert(*window != NULL, "Could not malloc the window!\n");
    if (get_value_config_bool(window_config, "RENDERER_PRESENTVSYNC", &vsync) == CONFIG_SUCCESS && vsync)
        *fps = 0;
    else {
        if (get_value_config_num(window_config, "WINDOW_MAX_FPS", &f_fps) != CONFIG_SUCCESS)
            f_fps = 60;
        *fps = (int) f_fps;
    }
}

void free_window(t_window *window)
{
    SDL_DestroyRenderer(window->SDLrenderer);
    SDL_DestroyWindow(window->SDLwindow);
    SDL_Quit();
    free(window);
}