#include "data_structures/linked_list.h"

#include "libft.h"

t_linked_list *new_node_linked_list(void *data, t_linked_list *prev, t_linked_list *next)
{
	t_linked_list *node;

	node = ft_memalloc(sizeof(t_linked_list));
	if (!node)
		return NULL;
	node->data = data;
	node->next = next;
	node->prev = prev;
	return node;
}

void free_node_linked_list(t_linked_list *list, bool free_data)
{
	if (free_data)
		free(list->data);
	free(list);
}

void free_linked_list(t_linked_list *list, bool free_data)
{
	t_linked_list *next;
	while (list)
	{
		next = list->next;
		if (free_data)
			free(list->data);
		free(list);
		list = next;
	}
}