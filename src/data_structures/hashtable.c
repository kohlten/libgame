#include "data_structures/hashtable.h"

#include "libft.h"
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>

hashtable_error init_hashtable(t_hashtable *table, long size) {
    if (size <= 0)
        size = 1;
    if (!table)
        return HASH_VALUE_ERROR;
    table->size = size;
    table->list = ft_memalloc(sizeof(t_hashtable_node *) * (size + 1));
    table->nodes = 0;
    table->collisions = 0;
    if (!table->list)
        return HASH_MALLOC_ERROR;
    return HASH_SUCCESS;
}

static inline u_int32_t murmur3_32(const u_int8_t *key, size_t len, u_int32_t seed) {
    u_int32_t h = seed;
    if (len > 3) {
        const u_int32_t *key_x4 = (const u_int32_t *) key;
        size_t i = len >> 2;
        do {
            u_int32_t k = *key_x4++;
            k *= 0xcc9e2d51;
            k = (k << 15) | (k >> 17);
            k *= 0x1b873593;
            h ^= k;
            h = (h << 13) | (h >> 19);
            h = (h * 5) + 0xe6546b64;
        } while (--i);
        key = (const u_int8_t *) key_x4;
    }
    if (len & 3) {
        size_t i = len & 3;
        u_int32_t k = 0;
        key = &key[i - 1];
        do {
            k <<= 8;
            k |= *key--;
        } while (--i);
        k *= 0xcc9e2d51;
        k = (k << 15) | (k >> 17);
        k *= 0x1b873593;
        h ^= k;
    }
    h ^= len;
    h ^= h >> 16;
    h *= 0x85ebca6b;
    h ^= h >> 13;
    h *= 0xc2b2ae35;
    h ^= h >> 16;
    return h;
}

hashtable_error add_node_hashtable(t_hashtable *table, char *key, void *data) {
    long long hash;
    t_hashtable_node *node;
    t_hashtable_node *tmp;

    if (!table || !key || !data)
        return HASH_VALUE_ERROR;
    hash = murmur3_32((u_int8_t *) key, ft_strlen(key), 5381) % table->size;
    if (hash == -1)
        return HASH_EMPTY_KEY;
    node = ft_memalloc(sizeof(t_hashtable_node));
    if (!node)
        return HASH_MALLOC_ERROR;
    node->data = data;
    node->key = strdup(key);
    if (!node->key)
        return HASH_MALLOC_ERROR;
    if (table->list[hash]) {
        tmp = table->list[hash];
        while (tmp->next) {
            if (strcmp(tmp->key, key) == 0) {
                free(node->key);
                free(node);
                return HASH_DUPLICATE_DATA;
            }
            tmp = tmp->next;
        }
        tmp->next = node;
        tmp->next->prev = tmp;
        table->collisions++;
    } else
        table->list[hash] = node;
    table->nodes++;
    if (table->nodes > table->size) {
        return resize_hashtable(table, table->size * 2);
    }
    return HASH_SUCCESS;
}

hashtable_error set_node_hastable(t_hashtable *table, char *key, void *data, int free_data, void (*free_func)(void *)) {
    long long hash;
    t_hashtable_node *tmp;

    if (!table || !key || !data)
        return HASH_VALUE_ERROR;
    hash = murmur3_32((u_int8_t *) key, ft_strlen(key), 5381) % table->size;
    if (hash == -1)
        return HASH_EMPTY_KEY;
    if (table->list[hash]) {
        tmp = table->list[hash];
        while (tmp) {
            if (strcmp(tmp->key, key) == 0) {
                if (free_data) {
                    if (free_func)
                        free_func(tmp->data);
                    else
                        free(tmp->data);
                }
                tmp->data = data;
                return HASH_SUCCESS;
            }
            tmp = tmp->next;
        }
        return HASH_NOT_FOUND;
    } else
        return HASH_NOT_FOUND;
}

hashtable_error get_node_hashtable(t_hashtable *table, char *key, void **data) {
    long hash;
    t_hashtable_node *node;

    if (!table || !data || !key)
        return HASH_VALUE_ERROR;
    hash = murmur3_32((u_int8_t *) key, ft_strlen(key), 5381) % table->size;
    if (hash == -1)
        return HASH_EMPTY_KEY;
    node = table->list[hash];
    while (node) {
        if (strcmp(key, node->key) == 0) {
            *data = node->data;
            return HASH_SUCCESS;
        }
        node = node->next;
    }
    return HASH_NOT_FOUND;
}


// @TODO Appears to segfault on resizing after calling this function
hashtable_error remove_node_hashtable(t_hashtable *table, char *key, bool free_data, void (*free_func)(void *ptr)) {
    long hash;
    t_hashtable_node *node;
    t_hashtable_node *prev;
    t_hashtable_node *next;

    if (!table)
        return HASH_VALUE_ERROR;
    hash = murmur3_32((u_int8_t *) key, ft_strlen(key), 5381) % table->size;
    if (hash == -1)
        return HASH_EMPTY_KEY;
    node = table->list[hash];
    prev = NULL;
    next = NULL;
    while (node) {
        if (node)
            next = node->next;
        else
            next = NULL;
        if (strcmp(node->key, key) == 0)
            break;
        prev = node;
        node = node->next;
    }
    if (node) {
        if (node == table->list[hash])
            table->list[hash] = next;
        if (prev)
            prev->next = next;
        free(node->key);
        if (free_data) {
            if (free_func)
                free_func(node->data);
            else
                free(node->data);
        }
        free(node);
        return HASH_SUCCESS;
    }
    return HASH_NOT_FOUND;
}

void free_hashtable(t_hashtable *table, bool free_data, void (*free_func)(void *ptr)) {
    t_hashtable_node *current;
    t_hashtable_node *next;

    for (int i = 0; i < table->size; i++) {
        if (table->list[i]) {
            current = table->list[i];
            next = current->next;
            while (current) {
                free(current->key);
                if (free_data) {
                    if (free_func)
                        free_func(current->data);
                    else
                        free(current->data);
                }
                free(current);
                current = next;
                if (current)
                    next = current->next;
            }
        }
    }
    free(table->list);
}

hashtable_error get_node_lengths_hashtable(t_hashtable *table, int **ptr) {
    int *lengths;
    t_hashtable_node *node;

    if (!table)
        return HASH_VALUE_ERROR;
    lengths = ft_memalloc(sizeof(int) * table->size);
    if (!lengths)
        return HASH_MALLOC_ERROR;
    for (int i = 0; i < table->size; i++) {
        node = table->list[i];
        while (node) {
            lengths[i]++;
            node = node->next;
        }
    }
    *ptr = lengths;
    return HASH_SUCCESS;
}

hashtable_error get_items_hashtable(t_hashtable *table, t_hashtable_node ***ptr) {
    t_hashtable_node **items;
    t_hashtable_node *node;
    int pos;

    if (!table)
        return HASH_VALUE_ERROR;
    items = ft_memalloc(sizeof(t_hashtable_node *) * table->nodes);
    if (!items)
        return HASH_MALLOC_ERROR;
    pos = 0;
    for (int i = 0; i < table->size; i++) {
        node = table->list[i];
        while (node) {
            items[pos] = node;
            node = node->next;
            pos++;
        }
    }
    *ptr = items;
    return HASH_SUCCESS;
}

hashtable_error resize_hashtable(t_hashtable *table, long new_size) {
    t_hashtable_node **items;
    hashtable_error code;
    long nodes;

    code = get_items_hashtable(table, &items);
    if (code != HASH_SUCCESS)
        return code;
    if (new_size <= 0)
        new_size = table->size * 2;
    free(table->list);
    table->list = ft_memalloc(sizeof(t_hashtable_node *) * (new_size + 1));
    if (!table->list)
        return HASH_MALLOC_ERROR;
    nodes = table->nodes;
    table->nodes = 0;
    table->collisions = 0;
    table->size = new_size;
    for (int i = 0; i < nodes; i++) {
        code = add_node_hashtable(table, items[i]->key, items[i]->data);
        if (code != HASH_SUCCESS)
            return code;
        free(items[i]->key);
        free(items[i]);
    }
    free(items);
    return HASH_SUCCESS;
}

int node_in_hashtable(t_hashtable *table, char *key) {
    long hash;
    t_hashtable_node *node;

    if (!table || !key)
        return HASH_VALUE_ERROR;
    hash = murmur3_32((u_int8_t *) key, ft_strlen(key), 5381) % table->size;
    if (hash == -1)
        return HASH_EMPTY_KEY;
    node = table->list[hash];
    while (node) {
        if (strcmp(key, node->key) == 0)
            return 1;
        node = node->next;
    }
    return 0;
}