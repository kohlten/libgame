//
// Created by Alexander Strole on 12/3/18.
//

#include "data_structures/stack.h"
#include "libft.h"

t_stack *new_stack()
{
    t_stack *stack;

    stack = ft_memalloc(sizeof(t_stack));
    if (!stack)
        return (NULL);
    stack->len = 0;
    return (stack);
}

void *pop_stack(t_stack *stack)
{
    t_linked_list *node;
    void *data;

    if (!stack || !stack->list)
        return (NULL);
    node = stack->list;
    stack->list = stack->list->next;
    data = node->data;
    free(node);
    return (data);
}

int push_stack(t_stack *stack, void *elem)
{
    t_linked_list *node;

    if (!stack)
        return (-1);
    node = new_node_linked_list(elem, NULL, NULL);
    if (!node)
        return (-1);
    if (!stack->list)
        stack->list = node;
    else {
        node->next = stack->list;
        stack->list->prev = node;
        stack->list = node;
    }
    stack->len++;
    return (0);
}

int get_len_stack(t_stack *stack)
{
    if (stack)
        return (stack->len);
    return (-1);
}

void free_stack(t_stack *stack, int free_info, void (*free_func)(void *data)) {
    t_linked_list *node;
    t_linked_list *next;

    if (stack) {
        node = stack->list;
        while (node != NULL) {
            next = node->next;
            if (free_info)
            {
                if (free_func)
                    free_func(node->data);
                else
                    free(node->data);
            }
            free(node);
            node = next;
        }
        free(stack);
    }
}
