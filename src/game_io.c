#include "game_io.h"

#include "libft.h"

#include <stdlib.h>
#include <errno.h>
#include <stdio.h>

char *read_file(char *name)
{
	FILE	*f;
	char	*string;
	long	fsize;
	long	rsize;

	if (!name || ft_strcmp(name, "") == 0)
	{
		printf("ERROR: Trying to read a file with an invalid name!\n");
		return NULL;
	}
	// Open the file and get a FILE struct
	f = fopen(name, "rb");
	if (!f)
	{
		// Later replace with logger
		fprintf(stderr, "Failed to open the file %s with error %d\n", name, errno);
		return NULL;
	}
	// Seek the end of the file
	// Get the bytes that it passed over
	// and then seek back to the beggining
	fseek(f, 0, SEEK_END);
	fsize = ftell(f);
	fseek(f, 0, SEEK_SET);

	// Malloc a string of the size of the buffer
	// Write the total size into the buffer
	string = ft_strnew(fsize);
	rsize = fread(string, sizeof(char), fsize, f);
	if (rsize != fsize)
	{
		printf("READ FILE: Checksum failed when reading from file!\n");
		free(string);
		fclose(f);
		return NULL;
	}
	fclose(f);

	string[fsize] = 0;
	return string;
}

char *get_path_of_file(char *path)
{
    int i;
    char *new;

    if (!path)
    	return (NULL);
    for (i = ft_strlen(path) - 1; i > 0 && !(path[i] == '/' || path[i] == '\\'); i--)
    	;
    if (i == 0)
    	return (NULL);
    new = ft_strnew(i + 1);
    if (!new)
    	return (NULL);
    ft_strncpy(new, path, i + 1);
	return (new);
}