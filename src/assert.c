//
// Created by Alexander Strole on 12/1/18.
//

#include "assert.h"
#include <stdlib.h>
#include <stdio.h>

void assert(int b, char *message) {
    if (!b) {
        printf("%s", message);
        exit(-1);
    }
}