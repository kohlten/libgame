#include "fps.h"

#include <stdio.h>
#include <time.h>
#include <SDL.h>

void init_fps(t_fps *fps, int target_fps) {
	fps->previous_time = 0;
	fps->frames = 0;
	fps->start_time = 0;
	// Calculate how many ticks we need to wait
	// For each frame(1000 MS = 1 SEC)
	if (target_fps > 0)
		fps->ticks_per_frame = 1000 / target_fps;
	else
		fps->ticks_per_frame = 0;
	fps->current_fps = 0;
}

void update_fps(t_fps *fps)
{
	int current;

	if (fps->previous_time == 0)
		fps->previous_time = SDL_GetTicks();
	// Get the current time in ticks
	current = SDL_GetTicks();
	// If the current minus the previous time is greater than 1 second
	// Set the previous time and return the frames
	if (current - fps->previous_time >= 1000) {
		fps->previous_time = SDL_GetTicks();
		fps->current_fps = fps->frames;
		fps->frames = 0;
	}
	fps->frames++;
}

int get_fps(t_fps *fps) {
	return fps->current_fps;
}

void limit_fps(t_fps *fps) {
	int frameTicks;

	if (fps->ticks_per_frame > 0) {
		if (fps->start_time == 0) {
			fps->start_time = SDL_GetTicks();
			return;
		}
		// Get how many ticks have passed in this frame
		frameTicks = SDL_GetTicks() - fps->start_time;
		// If we ran the frame faster than the time
		if (frameTicks < fps->ticks_per_frame) {
			// Delay the remaining time
			SDL_Delay(fps->ticks_per_frame - frameTicks);
		}
		fps->start_time = SDL_GetTicks();
	}
}


