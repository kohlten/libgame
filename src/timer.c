//
// Created by kohlten on 1/4/19.
//

#include "timer.h"

void init_timer(t_timer *timer, uint32_t target_time) {
    if (timer->inited)
        return;
    timer->started = false;
    timer->target_time = target_time;
    timer->inited = 1;
}

void start_timer(t_timer *timer) {
    if (!timer->inited)
        return;
    timer->started = true;
    timer->start_time = SDL_GetTicks();
}

void stop_timer(t_timer *timer) {
    if (!timer->inited)
        return;
    timer->started = false;
}

void reset_timer(t_timer *timer) {
    if (!timer->inited)
        return;
    stop_timer(timer);
    start_timer(timer);
}

Uint32 get_time_timer(t_timer *timer) {
    if (timer ->inited && timer->started)
        return SDL_GetTicks() - timer->start_time;
    else
        return 0;
}

bool timer_done(t_timer *timer) {
    return !timer->started;
}

bool timer_running(t_timer *timer) {
    return timer->started;
}

void update_timer(t_timer *timer) {
    if (!timer->inited)
        return;
    if (timer->started) {
        if (timer->target_time > 0 && SDL_GetTicks() - timer->start_time >= timer->target_time)
            timer->started = false;
    }
}

void wait_time(Uint32 time) {
    int start_time;

    start_time = SDL_GetTicks();
    while (SDL_GetTicks() - start_time < start_time + time)
        ;
}