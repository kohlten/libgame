#include "surface.h"

#include <string.h>
#include <stdio.h>
#include <SDL2_rotozoom.h>

#include "libft.h"

static SDL_Surface *load_image_surface(const char *name) {
    SDL_Surface *outputSurface;
    SDL_Surface *loadedSurface;

    loadedSurface = IMG_Load(name);
    if (!loadedSurface) {
        printf("Unable to load image %s SDL Error: %s\n", name, SDL_GetError());
        return NULL;
    }
    outputSurface = SDL_ConvertSurface(loadedSurface, loadedSurface->format, 0);
    SDL_FreeSurface(loadedSurface);
    if (!outputSurface) {
        printf("Failed to optimize surface %s! SDL Error: %s\n", name, SDL_GetError());
        return NULL;
    }
    return outputSurface;
}

t_surface *new_surface(const char *name) {
    t_surface *surface;

    surface = ft_memalloc(sizeof(t_surface));
    if (!surface) return NULL;
    surface->surface = load_image_surface(name);
    if (!surface->surface) return NULL;
    surface->format = surface->surface->format;
    surface->size = new_vector2i(surface->surface->w, surface->surface->h);
    return surface;
}

t_surface *new_unset_surface() {
    t_surface *surface;

    surface = ft_memalloc(sizeof(t_surface));
    if (!surface) return NULL;
    return surface;
}

t_surface *new_blank_surface(t_vector2i size, SDL_PixelFormat *format) {
    t_surface *surface;
    SDL_Surface *tmp;

    surface = ft_memalloc(sizeof(t_surface));
    if (!surface) return NULL;
    tmp = SDL_CreateRGBSurface(0, size.x, size.y, 32, 0, 0, 0, 0);
    if (!tmp) return NULL;
    if (format)
        surface->surface = SDL_ConvertSurface(tmp, format, 0);
    else
        surface->surface = SDL_ConvertSurface(tmp, tmp->format, 0);
    if (!surface->surface) return NULL;
    SDL_FreeSurface(tmp);
    wipe_surface(surface, SDL_MapRGBA(surface->surface->format, 255, 255, 255, 0));
    surface->format = surface->surface->format;
    surface->size = size;
    return surface;
}

t_surface *new_from_surface(SDL_Surface *sdlsurface) {
    t_surface *surface;

    surface = ft_memalloc(sizeof(t_surface));
    if (!surface) return NULL;
    surface->surface = SDL_ConvertSurface(sdlsurface, sdlsurface->format, SDL_SWSURFACE);
    if (!surface->surface) return NULL;
    surface->format = surface->surface->format;
    surface->size = new_vector2i(sdlsurface->w, sdlsurface->h);
    return surface;
}

int resize_surface(t_surface *surface, t_vector2i new_size, t_vector2i pos) {
    SDL_Surface *new_surface;
    SDL_Rect output_rect;
    int out;

    new_surface = SDL_CreateRGBSurface(0, new_size.x, new_size.y, 32, 0, 0, 0, 0);
    if (!new_surface) {
        printf("Failed to create surface! SDL Error: %s\n", SDL_GetError());
        return -1;
    }
    output_rect.x = pos.x;
    output_rect.y = pos.y;
    output_rect.w = new_size.x;
    output_rect.h = new_size.y;
    out = SDL_BlitScaled(surface->surface, NULL, new_surface, &output_rect);
    if (out < 0) {
        printf("Failed to blit surface! SDL Error: %s\n", SDL_GetError());
        SDL_FreeSurface(new_surface);
        return -1;
    }
    SDL_FreeSurface(surface->surface);
    surface->surface = new_surface;
    return 0;
}

void wipe_surface(t_surface *surface, int color) {
    SDL_FillRect(surface->surface, NULL, color);
}

uint32_t get_pixel(t_surface *surface, t_vector2i pos) {
    Uint8 *p;
    int bpp;

    if (pos.x < 0 || pos.y < 0 || pos.x > surface->surface->w || pos.y > surface->surface->h)
        return (0);
    bpp = surface->surface->format->BytesPerPixel;
    p = (Uint8 *) surface->surface->pixels + pos.y * surface->surface->pitch + pos.x * bpp;
    if (bpp == 1)
        return (*p);
    if (bpp == 2)
        return (*(Uint16 *) p);
    if (bpp == 3)
        return (p[0] | p[1] << 8 | p[2] << 16);
    if (bpp == 4)
        return (*(Uint32 *) p);
    else
        return (0);
}

void set_pixel(t_surface *surface, t_vector2i pos, uint32_t pixel) {
    Uint8 *p;
    int bpp;

    if (pos.x < 0 || pos.y < 0 || pos.x > surface->surface->w || pos.y > surface->surface->h)
        return;
    if (SDL_MUSTLOCK(surface->surface))
        SDL_LockSurface(surface->surface);
    bpp = surface->surface->format->BytesPerPixel;
    p = (Uint8 *) surface->surface->pixels + pos.y * surface->surface->pitch + pos.x * bpp;
    if (bpp == 1)
        *p = pixel;
    if (bpp == 2)
        *(Uint16 *) p = pixel;
    if (bpp == 3) {
        p[0] = pixel & 0xff;
        p[1] = (pixel >> 8) & 0xff;
        p[2] = (pixel >> 16) & 0xff;
    }
    if (bpp == 4)
        *(Uint32 *) p = pixel;
    if (SDL_MUSTLOCK(surface->surface))
        SDL_UnlockSurface(surface->surface);
}

int draw_surface(t_surface *surface, SDL_Renderer *renderer, t_vector2i pos) {
    t_vector2i size;
    SDL_Texture *text = NULL;

    text = SDL_CreateTextureFromSurface(renderer, surface->surface);
    if (!text)
        return -1;
    size.x = surface->surface->w;
    size.y = surface->surface->h;
    SDL_RenderCopy(renderer, text, NULL,
                   &(SDL_Rect) {pos.x, pos.y, size.x, size.y});
    SDL_DestroyTexture(text);
    return 0;
}

t_surface *rotate_surface(t_surface *surface, double angle, double zoom, bool smooth) {
    SDL_Surface *rotated;
    t_surface *new;

    rotated = rotozoomSurface(surface->surface, angle, zoom, smooth);
    if (!rotated) return NULL;
    new = new_from_surface(rotated);
    SDL_FreeSurface(rotated);
    if (!new) return NULL;
    return new;
}

int rotate_and_draw_surface(t_surface *surface, SDL_Renderer *renderer, t_vector2i pos, double angle, double zoom, bool smooth) {
    t_surface *tmp_surf;

    tmp_surf = rotate_surface(surface, angle, zoom, smooth);
    if (!tmp_surf) return -1;
    draw_surface(tmp_surf, renderer, pos);
    free_surface(tmp_surf);
    return 0;
}


void free_surface(void *p) {
    t_surface *surface;

    surface = (t_surface *)p;
    SDL_FreeSurface(surface->surface);
    free(surface);
}