//
// Created by Alexander Strole on 12/2/18.
//

#include "random.h"
#include "vector/vector.h"
#include <sodium.h>

double random_double()
{
    return randombytes_random() / (0xffffffff / 1.);
}

int randint(int low, int upper)
{
    return (map(randombytes_random(),
                new_vector2f(0, 0xffffffff), new_vector2f(low, upper)));
}