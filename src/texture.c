//
// Created by Alexander Strole on 12/19/18.
//

#include "texture.h"
#include <SDL2_rotozoom.h>
#include "libft.h"
#include "surface.h"

static SDL_Texture *load_image_texture(const char *name, SDL_Renderer *renderer)
{
    SDL_Texture *output_texture;
    SDL_Surface *loaded_surface;

    loaded_surface = IMG_Load(name);
    if (!loaded_surface)
    {
        printf("Unable to load image %s SDL Error: %s\n", name, SDL_GetError());
        return NULL;
    }
    output_texture = SDL_CreateTextureFromSurface(renderer, loaded_surface);
    SDL_FreeSurface(loaded_surface);
    if (!output_texture)
    {
        printf("Failed to optimize surface %s! SDL Error: %s\n", name, SDL_GetError());
        return NULL;
    }
    return output_texture;
}

t_texture *new_texture(SDL_Renderer *renderer, char *filename) {
    t_texture *texture;

    texture = ft_memalloc(sizeof(t_texture));
    if (!texture) return NULL;
    if (filename) {
        if (load_new_texture(texture, filename, renderer) != 0)
            return NULL;
    }
    return texture;
}

t_texture *new_texture_from_surface(SDL_Renderer *renderer, t_surface *surface) {
    t_texture *texture;

    texture = ft_memalloc(sizeof(t_texture));
    if (!texture) return NULL;
    texture->texture = SDL_CreateTextureFromSurface(renderer, surface->surface);
    if (!texture->texture) return NULL;
    texture->width = surface->size.x;
    texture->height = surface->size.y;
    return texture;
}

void set_color_texture(t_texture *texture, SDL_Color color) {
    SDL_SetTextureColorMod(texture->texture, color.r, color.g, color.b);
    SDL_SetTextureAlphaMod(texture->texture, color.a);
}

void set_blend_texture(t_texture *texture, SDL_BlendMode mode) {
    SDL_SetTextureBlendMode(texture->texture, mode);
}

void set_texture(t_texture *texture, SDL_Texture *sdltexture) {
    if (texture->texture)
        SDL_DestroyTexture(texture->texture);
    texture->texture = sdltexture;
    SDL_QueryTexture(texture->texture, NULL, NULL, &texture->width, &texture->height);
}

int load_new_texture(t_texture *texture, char *filename, SDL_Renderer *renderer) {
    if (texture->texture)
        SDL_DestroyTexture(texture->texture);
    texture->texture = load_image_texture(filename, renderer);
    if (!texture->texture) return -1;
    SDL_QueryTexture(texture->texture, NULL, NULL, &texture->width, &texture->height);
    return 0;
}

void render_texture(t_texture *texture, SDL_Renderer *renderer, t_vector2i pos, SDL_Rect *clip, double angle,
                    t_vector2i center, SDL_RendererFlip flip) {
    SDL_Rect render_quad;

    if (!texture->texture)
        return;
    render_quad.x = pos.x;
    render_quad.y = pos.y;
    if (clip != NULL) {
        render_quad.w = clip->w;
        render_quad.h = clip->h;
    } else {
        render_quad.w = texture->width;
        render_quad.h = texture->height;
    }
    SDL_RenderCopyEx(renderer, texture->texture, clip, &render_quad, angle, &(SDL_Point) {center.x, center.y}, flip);
}

/*int resize_texture(t_texture *texture, SDL_Renderer *renderer, t_vector2i new_size) {
    SDL_Texture *new_texture = NULL;
    Uint32 format;

    SDL_QueryTexture(texture->texture, &format, NULL, NULL, NULL);
    new_texture = SDL_CreateTexture(renderer, format, SDL_TEXTUREACCESS_TARGET, new_size.x, new_size.y);
    if (new_texture == NULL)
        return -1;
    SDL_SetRenderTarget(renderer, new_texture);
    SDL_RenderCopy(renderer, texture->texture, NULL, &(SDL_Rect){0, 0, new_size.x, new_size.y});
    SDL_SetRenderTarget(renderer, NULL);
    SDL_DestroyTexture(texture->texture);
    texture->texture = new_texture;
    return 0;
}*/

void free_texture(t_texture *texture) {
    SDL_DestroyTexture(texture->texture);
    free(texture);
}

int load_textures(vector *tex_arr, SDL_Renderer *renderer, char **filenames, int files) {
    t_texture *tex;
    int i;

    i = 0;
    while (i <= files) {
        tex = new_texture(renderer, filenames[i]);
        if (!tex)
            return -1;
        vector_add(tex_arr, tex);
        i++;
    }
    return 0;
}