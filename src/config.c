//
// Created by Alexander Strole on 12/7/18.
//

#include "config.h"
#include "game_io.h"
#include "libft.h"
#include "vector/vector.h"
#include <SDL.h>


// @TODO Switch the return code with the return value

static int get_twod_char_len(char **str)
{
    int i;

    i = 0;
    while (str[i])
        i++;
    return (i);
}

static void free_twod_char(char **str)
{
    int i;
    int len;

    len = get_twod_char_len(str);
    for (i = 0; i < len; i++)
        free(str[i]);
    free(str);
}

static int is_num(char *str)
{
    int i;

    for (i = 0; str[i]; i++) {
        if (!ft_isdigit(str[i]) && str[i] != '.')
            return 0;
    }
    return 1;
}

static int is_bool(char *str)
{
    return (ft_strcmp(str, "TRUE") == 0 || ft_strcmp(str, "FALSE") == 0);
}

static int is_vector(char *str, int dimentions)
{
    int i;
    char **split;
    int out;

    i = 0;
    out = 1;
    split = ft_strsplit(str, ',');
    if (!split)
        out = 0;
    if (out == 1 && get_twod_char_len(split) != dimentions)
        out = 0;
    if (out == 1)
        for (i = 0; split[i]; i++)
            if (!is_num(split[i]))
                out = 0;
    if (split)
        for (i = 0; split[i]; i++)
            free(split[i]);
                free(split);
    return out;
}

static t_config_error read_from_config(t_config *config)
{
    char *text;
    char **lines;
    char **line;
    char **vector_nums;
    t_config_node *c_node;
    int i;

    text = read_file(config->filename);
    if (!text) return CONFIG_FILE_ERROR;
    lines = ft_strsplit(text, '\n');
    if (!lines) return CONFIG_MALLOC_ERROR;
    for (i = 0; lines[i]; i++) {
        if (lines[i][0] == '#') continue;
        line = ft_strsplit(lines[i], ' ');
        if (!line) return CONFIG_MALLOC_ERROR;
        if (get_twod_char_len(line) < 2) return CONFIG_FILE_ERROR;
        c_node = ft_memalloc(sizeof(t_config_node));
        if (!c_node) return CONFIG_MALLOC_ERROR;
        if (is_bool(line[1])) {
            c_node->type = CONFIG_BOOL;
            c_node->info = ft_memalloc(sizeof(int));
            if (!c_node->info) return CONFIG_MALLOC_ERROR;
            if (ft_strcmp(line[1], "TRUE") == 0) *((int *) c_node->info) = 1;
            else *((int *) c_node->info) = 0;
        } else if (is_num(line[1])) {
            c_node->type = CONFIG_NUM;
            c_node->info = ft_memalloc(sizeof(double));
            if (!c_node->info) return CONFIG_MALLOC_ERROR;
            *((double *) c_node->info) = ft_atof(line[1]);
        } else if (is_vector(line[1], 2)) {
            c_node->type = CONFIG_VECTOR2;
            c_node->info = ft_memalloc(sizeof(t_vector2f));
            if (!c_node->info) return CONFIG_MALLOC_ERROR;
            vector_nums = ft_strsplit(line[1], ',');
            if (!vector_nums) return CONFIG_MALLOC_ERROR;
            ((t_vector2f *) c_node->info)->x = ft_atof(vector_nums[0]);
            ((t_vector2f *) c_node->info)->y = ft_atof(vector_nums[1]);
            free_twod_char(vector_nums);
        } else if (is_vector(line[1], 4)) {
            c_node->type = CONFIG_COLOR;
            c_node->info = ft_memalloc(sizeof(SDL_Color));
            if (!c_node->info) return CONFIG_MALLOC_ERROR;
            vector_nums = ft_strsplit(line[1], ',');
            if (!vector_nums) return CONFIG_MALLOC_ERROR;
            ((SDL_Color *) c_node->info)->r = (int)ft_atoi(vector_nums[0]);
            ((SDL_Color *) c_node->info)->g = (int)ft_atoi(vector_nums[1]);
            ((SDL_Color *) c_node->info)->b = (int)ft_atoi(vector_nums[2]);
            ((SDL_Color *) c_node->info)->a = (int)ft_atoi(vector_nums[3]);
            free_twod_char(vector_nums);
        } else {
            return CONFIG_FILE_ERROR;
        }
        if (add_node_hashtable(&config->table, line[0], c_node) != HASH_SUCCESS) return CONFIG_HASTABLE_ERROR;
        free_twod_char(line);
    }
    free_twod_char(lines);
    free(text);
    return CONFIG_SUCCESS;
}

t_config_error init_config(t_config *config, char *filename, int hash_len)
{
    if (!config) return CONFIG_VALUE_ERROR;
    config->filename = ft_strdup(filename);
    if (!config->filename) return CONFIG_MALLOC_ERROR;
    if (init_hashtable(&config->table, hash_len) != 0) return CONFIG_HASTABLE_ERROR;
    config->initialized = 1;
    return read_from_config(config);
}

t_config_error get_value_config_bool(t_config *config, char *key, bool *b)
{
    t_config_node *c_node;

    if (!config || config->initialized == 0 || key == NULL)
        return CONFIG_VALUE_ERROR;
    if (get_node_hashtable(&config->table, key, (void **)&c_node) != HASH_SUCCESS) return CONFIG_HASTABLE_ERROR;
    if (c_node->type != CONFIG_BOOL) return CONFIG_VALUE_ERROR;
    *b = *(int *)c_node->info;
    return CONFIG_SUCCESS;
}

t_config_error get_value_config_num(t_config *config, char *key, float *b)
{
    t_config_node *c_node;

    if (!config || config->initialized == 0 || key == NULL)
        return CONFIG_VALUE_ERROR;
    if (get_node_hashtable(&config->table, key, (void **)&c_node) != HASH_SUCCESS) return CONFIG_HASTABLE_ERROR;
    if (c_node->type != CONFIG_NUM) return CONFIG_VALUE_ERROR;
    *b = *(double *)c_node->info;
    return CONFIG_SUCCESS;
}

t_config_error get_value_config_vector(t_config *config, char *key, t_vector2f *vec)
{
    t_config_node *c_node;

    if (!config || config->initialized == 0 || key == NULL)
        return CONFIG_VALUE_ERROR;
    if (get_node_hashtable(&config->table, key, (void **)&c_node) != HASH_SUCCESS) return CONFIG_HASTABLE_ERROR;
    if (c_node->type != CONFIG_VECTOR2) return CONFIG_VALUE_ERROR;
    vec->x = ((t_vector2f *)c_node->info)->x;
    vec->y = ((t_vector2f *)c_node->info)->y;
    return CONFIG_SUCCESS;
}

t_config_error get_value_config_color(t_config *config, char *key, SDL_Color *color)
{
    t_config_node *c_node;

    if (!config || config->initialized == 0 || key == NULL)
        return CONFIG_VALUE_ERROR;
    if (get_node_hashtable(&config->table, key, (void **)&c_node) != HASH_SUCCESS) return CONFIG_HASTABLE_ERROR;
    if (c_node->type != CONFIG_COLOR) return CONFIG_VALUE_ERROR;
    color->r = ((SDL_Color *)c_node->info)->r;
    color->g = ((SDL_Color *)c_node->info)->g;
    color->b = ((SDL_Color *)c_node->info)->b;
    color->a = ((SDL_Color *)c_node->info)->a;
    return CONFIG_SUCCESS;
}

t_config_error set_value_config_bool(t_config *config, char *key, int b)
{
    t_config_node *c_node;

    if (!config || config->initialized == 0 || key == NULL)
        return CONFIG_VALUE_ERROR;
    if (get_node_hashtable(&config->table, key, (void **)&c_node) != HASH_SUCCESS) return CONFIG_HASTABLE_ERROR;
    if (c_node->type != CONFIG_BOOL) return CONFIG_VALUE_ERROR;
    *((int *)c_node->info) = b;
    return CONFIG_SUCCESS;
}

t_config_error set_value_config_num(t_config *config, char *key, float num)
{
    t_config_node *c_node;

    if (!config || config->initialized == 0 || key == NULL)
        return CONFIG_VALUE_ERROR;
    if (get_node_hashtable(&config->table, key, (void **)&c_node) != HASH_SUCCESS) return CONFIG_HASTABLE_ERROR;
    if (c_node->type != CONFIG_NUM) return CONFIG_VALUE_ERROR;
    *((float *)c_node->info) = num;
    return CONFIG_SUCCESS;
}

static void free_config_node(void *data)
{
    t_config_node *node;

    node = (t_config_node *)data;
    free(node->info);
    free(node);
}

void free_config(t_config *config)
{
    free_hashtable(&config->table, 1, free_config_node);
    free(config->filename);
}