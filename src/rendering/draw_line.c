//
// Created by Alexander Strole on 12/1/18.
//

/*#include <SDL.h>
#include "vector/vector.h"
#include "rendering/draw_line.h"
#include "surface.h"

static void plot_point(int x, int y, SDL_Surface *surf, SDL_Color color)
{
    set_pixel(surf, (t_vector2i){x, y}, SDL_MapRGBA(surf->format, color.r, color.g, color.b, color.a));
}

static void draw_line_low(int x0, int y0, int x1, int y1, SDL_Surface *surf, SDL_Color color)
{
    double dx = x1 - x0;
    double dy = y1 - y0;
    double yi = 1;
    double d;
    double y;
    double x;

    if (dy < 0)
    {
        yi = -1;
        dy = -dy;
    }
    d = 2 * dy - dx;
    y = y0;
    for (x = x0; x < x1; x++)
    {
        plot_point(x, y, surf, color);
        if (d > 0)
        {
            y = y + yi;
            d = d - 2 * dx;
        }
        d = d + 2 * dy;
    }
}

static void draw_line_high(int x0, int y0, int x1, int y1, SDL_Surface *surf, SDL_Color color)
{
    double dx = x1 - x0;
    double dy = y1 - y0;
    double xi = 1;
    double d;
    double y;
    double x;

    if (dx < 0)
    {
        xi = -1;
        dx = -dx;
    }
    d = 2 * dx - dy;
    x = x0;
    for (y = y0; y < y1; y++)
    {
        plot_point(x, y, surf, color);
        if (d > 0)
        {
            x = x + xi;
            d = d - 2 * dy;
        }
        d = d + 2 * dx;
    }
}

void draw_line(int x0, int y0, int x1, int y1, SDL_Surface *surf, SDL_Color color)
{
    if (!surf)
        return ;
    if (abs(y1 - y0) < abs(x1 - x0))
    {
        if (x0 > x1)
            draw_line_low(x1, y1, x0, y0, surf, color);
        else
            draw_line_low(x0, y0, x1, y1, surf, color);
    }
    else
    {
        if (y0 > y1)
            draw_line_high(x1, y1, x0, y0, surf, color);
        else
            draw_line_low(x0, y0, x1, y1, surf, color);
    }
}*/