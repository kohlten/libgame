#include "rendering/text/text_font.h"
#include "libft.h"
#include <string.h>
#include <stdio.h>

// @Cleanme Maybe move to differnet location? util
SDL_Texture *load_from_rendered_text(SDL_Renderer *renderer, char *text, TTF_Font *font, SDL_Color color)
{
	SDL_Surface *text_surface;
	SDL_Texture *text_texture;

	if (!text || !font || !renderer)
		return NULL;
	text_surface = TTF_RenderText_Solid(font, text, color);
	if (!text_surface)
	{
		printf("Failed to render text to surface! SDL_tff Error: %s\n", TTF_GetError());
		return NULL;
	}
	text_texture = SDL_CreateTextureFromSurface(renderer, text_surface);
	if (!text_texture)
	{
		printf("Failed to create texture! SDL Error: %s\n", SDL_GetError());
		return NULL;
	}
	SDL_FreeSurface(text_surface);
	return text_texture;
}

// @Cleanme Maybe move to different location? util? game?
TTF_Font *load_font(char *name, int size)
{
	TTF_Font *font;

	if (!name)
		return NULL;
	font = TTF_OpenFont(name, size);
	if (!font)
	{
		printf("WARNING: Failed to open font %s!\n", name);
		return NULL;
	}
	return font;
}

t_text_font *change_text_string_font(t_text_font *text, SDL_Renderer *renderer, char *new)
{
	if (!text || !new)
	{
		printf("ERROR: Cannot change the text of a null text!\n");
		return NULL;
	}
	if (text->texture)
		SDL_DestroyTexture(text->texture);
	text->texture = load_from_rendered_text(renderer, new, text->font, text->color);
	if (!text->texture)
		printf("WANRING: Failed to set the new text!\n");
	SDL_QueryTexture(text->texture, NULL, NULL, &text->size.x, &text->size.y);
	return text;
}

// @Cleanme Change the order of these variables and see if we can remove a few
t_text_font *new_text_font(char *string, SDL_Renderer *renderer, TTF_Font *font, SDL_Color color, t_vector2i position)
{
	t_text_font *text;

	if (!renderer || !font)
	{
		printf("ERROR: Renderer or font null!\n");
		return NULL;
	}
	if (!string)
	{
		printf("EROROR: Creating text with empty string!\n");
		return NULL;
	}
	text = ft_memalloc(sizeof(t_text_font));
	if (!text)
		return NULL;
	text->font = font;
	if (!text->font)
	{
		free_text_font(text);
		return NULL;
	}
	if (strcmp(string, "") != 0)
		text = change_text_string_font(text, renderer, string);
	else
	{
		printf("ERROR: Trying to create a text with an empty string!\n");
		free_text_font(text);
		return NULL;
	}
	text->color = color;
	text->position = position;
	text->font_size = TTF_FontHeight(text->font);
	return text;
}

void render_text_font(t_text_font *text, SDL_Renderer *renderer)
{
	if (text->texture)
	{
		SDL_RenderCopy(renderer, text->texture, NULL,
						&(SDL_Rect){text->position.x, text->position.y, text->size.x, text->size.y});
	}
}

void free_text_font(t_text_font *text)
{
	if (text->texture)
		SDL_DestroyTexture(text->texture);
	free(text);
	text = NULL;
}
