//
// Created by kohlten on 1/14/19.
//

#include <jansson.h>

#include "rendering/text/text_collage.h"
#include "libft.h"
#include "game_io.h"

// @TODO Make these return enum errors

static int get_surfaces(t_text_collage *collage, char *file, char *order, SDL_PixelFormat *format) {
    t_surface *surf;
    t_surface *tmp;
    char key[2];
    int i;
    int j;
    int current_char;
    hashtable_error out;

    surf = new_surface(file);
    if (!surf) return -1;
    current_char = 0;
    key[1] = '\0';
    for (i = 0; i < surf->size.x; i += collage->size.x) {
        for (j = 0; j < surf->size.y; j += collage->size.y) {
            key[0] = order[current_char];
            tmp = new_blank_surface(collage->size, format);
            if (!tmp) return -1;
            SDL_BlitSurface(surf->surface, &(SDL_Rect){i, j, collage->size.x, collage->size.y}, tmp->surface, NULL);
            out = add_node_hashtable(&collage->characters, key, tmp);
            if (out == HASH_DUPLICATE_DATA) free_surface(tmp);
            else if (out != HASH_SUCCESS) return -1;
            current_char++;
        }
    }
    free_surface(surf);
    return 0;
}

int init_text_collage(t_text_collage *collage, t_vector2i size, char *file, char *order, SDL_PixelFormat *format) {
    if (!collage || !file || !order) return -1;
    ft_bzero(collage, sizeof(t_text_collage));
    collage->size = size;
    if (init_hashtable(&collage->characters, 30) != HASH_SUCCESS) return -1;
    if (get_surfaces(collage, file, order, format) != 0) return -1;
    collage->inited = true;
    return 0;
}

int init_text_collage_json(t_text_collage *collage, char *json_file, SDL_PixelFormat *format) {
    char *text, *file, *order, *path, *file_joined;
    json_t *root, *node;
    json_error_t error;

    if (!collage || !json_file) return -1;
    ft_bzero(collage, sizeof(t_text_collage));
    text = read_file(json_file);
    if (!text) return -1;
    root = json_loads(text, 0, &error);
    if (!root) {
        fprintf(stderr, "Json error! File: %s Error: line %d: %s\n", json_file, error.line, error.text);
        return -1;
    }
    node = json_object_get(root, "file");
    if (!json_is_string(node)) return -1;
    file = ft_strdup(json_string_value(node));
    if (!file) return -1;
    path = get_path_of_file(json_file);
    if (path) {
        file_joined = ft_strjoin(path, file);
        free(file);
        free(path);
        file = file_joined;
    }
    if (!file) return -1;
    node = json_object_get(root, "size");
    if (!json_is_array(node)) return -1;
    collage->size.x = (int)json_integer_value(json_array_get(node, 0));
    collage->size.y = (int)json_integer_value(json_array_get(node, 1));
    node = json_object_get(root, "order");
    if (!json_is_string(node)) return -1;
    order = ft_strdup(json_string_value(node));
    if (!order) return -1;
    if (init_hashtable(&collage->characters, 30) != HASH_SUCCESS) return -1;
    if (get_surfaces(collage, file, order, format) != 0) return -1;
    free(file);
    free(order);
    free(text);
    json_decref(root);
    collage->inited = true;
    return 0;
}

t_vector2i get_text_size_text_collage(t_text_collage *collage, char *text, double zoom) {
    int i;
    int len;
    t_vector2i size;

    if (!collage) return new_vector2i(-1, -1);
    if (!collage->inited) return new_vector2i(-1, -1);
    if (!text) return new_vector2i(-1, -1);
    len = (int)ft_strlen(text);
    size = vector2i_zero;
    for (i = 0; i < len; i++) {
        if (text[i] == '\n')
            size.y += collage->size.y * zoom;
        else
            size.x += collage->size.x * zoom;
    }
    if (len > 0) size.y += collage->size.y;
    return  size;
}

int draw_text_collage(t_text_collage *collage, char *text, SDL_Renderer *renderer, t_vector2i pos, double angle, double zoom, bool smooth, t_vector2i *offsets, int num_offsets) {
    t_surface *letter;
    char key[2];
    int i;
    t_vector2i current;

    if (!collage) return -1;
    if (!collage->inited) return -1;
    if (!text) return -1;
    if (offsets && num_offsets < (int)ft_strlen(text)) return -1;
    key[1] = 0;
    for (i = 0; i < (int)ft_strlen(text); i++) {
        if (text[i] == '\n') {
            pos.y += collage->size.y * zoom;
            continue;
        }
        if (text[i] == ' ') {
            pos.x += collage->size.x * zoom;
            continue;
        }
        key[0] = text[i];
        if (get_node_hashtable(&collage->characters, key, (void **)&letter) != HASH_SUCCESS) return -1;
        if (offsets)
            current = add_vector2i(pos, offsets[i]);
        else
            current = pos;
        rotate_and_draw_surface(letter, renderer, current, angle, zoom, smooth);
        pos.x += collage->size.y * zoom;
    }
    return 0;
}

void free_text_collage(t_text_collage *collage) {
    if (!collage->inited) return;
    free_hashtable(&collage->characters, true, free_surface);
}



