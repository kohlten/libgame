#include "rendering/text/text.h"

#include "libft.h"
#include <string.h>
#include <stdio.h>

t_text *new_text(char *string, SDL_Renderer *renderer, SDL_Color color,
				t_vector2i position, char *bitmap_json_name, TTF_Font *font)
{
	t_text *text;

	if (bitmap_json_name && font)
	{
		printf("ERROR: Cannot create both a bitmap and a font text!\n");
		return NULL;
	}
	if (!bitmap_json_name && !font)
	{
		printf("ERROR: Cannot create a text when I don't know what type to create!\n");
		return NULL;
	}
	text = ft_memalloc(sizeof(t_text));
	if (!text)
	{
		printf("ERROR: Failed to malloc text!\n");
		return NULL;
	}
	if (bitmap_json_name)
	{
		text->text_bitmap = new_text_bitmap(string, renderer, bitmap_json_name, color, position);
		if (!text->text_bitmap)
		{
			printf("Failed to create a text bitmap!\n");
			return NULL;
		}
		text->type = TYPE_BITMAP;
	}
	else if (font)
	{
		text->text_font = new_text_font(string, renderer, font, color, position);
		if (!text->text_font)
			return NULL;
		text->type = TYPE_FONT;
	}
	return text;
}

void render_text(t_text *text, SDL_Renderer *renderer)
{
	if (!text)
	{
		printf("WARNING: Trying to render a NULL text!\n");
		return;
	}
	if (text->type == TYPE_FONT)
		render_text_font(text->text_font, renderer);
	else if (text->type == TYPE_BITMAP)
		render_text_bitmap(text->text_bitmap, renderer);
	else
		printf("WARNING: Failed to display text due to malformed text struct!\n");
}

t_text *change_text_string(t_text *text, SDL_Renderer *renderer, char *new)
{
	if (text->type == TYPE_FONT)
		text->text_font = change_text_string_font(text->text_font, renderer, new);
	else if (text->type == TYPE_BITMAP)
		text->text_bitmap = change_text_string_bitmap(text->text_bitmap, new);
	else
		printf("WARNING: Failed to display text due to malformed text struct!\n");
	return text;
}

void free_text(t_text *text)
{
	if (!text)
	{
		printf("WARNING: Trying to free a NULL text!\n");
		return;
	}
	if (text->type == TYPE_FONT)
		free_text_font(text->text_font);
	else if (text->type == TYPE_BITMAP)
		free_text_bitmap(text->text_bitmap);
	free(text);
	text = NULL;
}