#include "rendering/text/glyph_table.h"

#include <jansson.h>
#include <string.h>
#include <stdio.h>
#include <time.h>

#include "game_io.h"
#include "libft.h"

glyph_table_error init_glyph_table(t_glyph_table *table, long size)
{

	if (size <= 0)
		size = 26;
	if (!table)
		return GLYPH_VALUE_ERROR;
	if (init_hashtable(&table->hashtable, size) != HASH_SUCCESS)
		return GLYPH_HASHTABLE_ERROR;
	return GLYPH_SUCCESS;
}

static glyph_table_error parse_glyph_table_values(t_glyph_table *table, json_t *root)
{
	json_t 		 *object;
	json_t		 *data;
	json_t		 *object_data;
	size_t		 i;
	char		 id[2];
	t_glyph_node *node;

	// Load in config variables
	object = json_object_get(root, "config");
	if (!json_is_object(object))  return GLYPH_JSON_ERROR;
	data = json_object_get(object, "textureFile");
	if (!json_is_string(data))    return GLYPH_JSON_ERROR;
	table->image_file_name = strdup(json_string_value(data));
	data = json_object_get(object, "charHeight");
	if (!json_is_integer(data))   return GLYPH_JSON_ERROR;
	table->font_size = json_integer_value(data);
	data = json_object_get(object, "charSpacing");
	if (!json_is_integer(data))   return GLYPH_JSON_ERROR;
	table->spacing = json_integer_value(data);

	// Actually load the symbols
	object = json_object_get(root, "symbols");
	if (!json_is_array(object))   return GLYPH_JSON_ERROR;
	id[1] = '\0';
	for (i = 0; i < json_array_size(object); i++)
	{
		data = json_array_get(object, i);
		if (!json_is_object(data)) return GLYPH_JSON_ERROR;
		object_data = json_object_get(data, "id");
		if (!json_is_integer(object_data)) return GLYPH_JSON_ERROR;
		id[0] = (char)json_integer_value(object_data);
		node = ft_memalloc(sizeof(t_glyph_node));
		if (!node) return GLYPH_MALLOC_ERROR;
		object_data = json_object_get(data, "width");
		if (!json_is_integer(object_data)) return GLYPH_JSON_ERROR;
		node->size.x = json_integer_value(object_data);
		object_data = json_object_get(data, "height");
		if (!json_is_integer(object_data)) return GLYPH_JSON_ERROR;
		node->size.y = json_integer_value(object_data);
		object_data = json_object_get(data, "x");
		if (!json_is_integer(object_data)) return GLYPH_JSON_ERROR;
		node->pos.x = json_integer_value(object_data);
		object_data = json_object_get(data, "y");
		if (!json_is_integer(object_data)) return GLYPH_JSON_ERROR;
		node->pos.y = json_integer_value(object_data);
		object_data = json_object_get(data, "xoffset");
		if (!json_is_integer(object_data)) return GLYPH_JSON_ERROR;
		node->offset.x = json_integer_value(object_data);
		object_data = json_object_get(data, "yoffset");
		if (!json_is_integer(object_data)) return GLYPH_JSON_ERROR;
		node->offset.y = json_integer_value(object_data);
		object_data = json_object_get(data, "xadvance");
		if (!json_is_integer(object_data)) return GLYPH_JSON_ERROR;
		node->xadvance = json_integer_value(object_data);
		if (add_node_hashtable(&table->hashtable, id, node) != HASH_SUCCESS) return GLYPH_HASHTABLE_ERROR;
	}
	return GLYPH_SUCCESS; 
}

glyph_table_error set_glyph_table_values(t_glyph_table *table, char *filename)
{
	char *text;
	int i;
	glyph_table_error code;
	json_t *root;
	json_error_t error;

//#ifdef PROFILE_CODE
//	clock_t start_time;
//
//    start_time = clock();
//#endif
	i = strlen(filename);
	// Go through the file name until we reach a dot
	while (i >= 0 && filename[i] != '.')
		i--;
	// If the file is not a json then exit
	if (ft_strcmp(filename, "") == 0 || ft_strcmp(filename + i, ".json") != 0)
		return GLYPH_VALUE_ERROR;
	if (!table || !filename)
		return GLYPH_VALUE_ERROR;
	text = read_file(filename);
	if (!text)
		return GLYPH_FILE_ERROR;
	root = json_loads(text, 0, &error);
	if (!root)
	{
		fprintf(stderr, "GLYPH TABLE: Json error: line %d: %s\n", error.line, error.text);
		return GLYPH_FILE_ERROR;
	}
	free(text);
	code = parse_glyph_table_values(table, root);
	json_decref(root);
//#ifdef PROFILE_CODE
//    printf("SETUP GLYPH TABLE: Took %Lf MS to complete!\n", ((clock() - start_time) / ((long double)CLOCKS_PER_SEC)) * 1000);
//#endif
	return code;
}

glyph_table_error get_glyph_table_node(t_glyph_table *table, char character, t_glyph_node **node)
{
	hashtable_error error;
	char string[2];

	string[0] = character;
	string[1] = '\0';
	error = get_node_hashtable(&table->hashtable, string, (void *)node);
	if (error != HASH_SUCCESS)
	{
		printf("Got error from hashtable! Error: %d\n", error);
		return GLYPH_HASHTABLE_ERROR;
	}
	return GLYPH_SUCCESS;
}

glyph_table_error remove_glyph_table_node(t_glyph_table *table, char character, void (*free_func)(void *ptr))
{
	char string[2];
	hashtable_error error;

	string[0] = character;
	string[1] = '\0';
	error = remove_node_hashtable(&table->hashtable, string, true, free_func);
	if (error != HASH_SUCCESS)
	{
		printf("Got error from hashtable! Error: %d\n", error);
		return GLYPH_HASHTABLE_ERROR;
	}
	return GLYPH_SUCCESS;
}

void free_glyph_table(t_glyph_table *table, void (*free_func)(void *ptr))
{
	free_hashtable(&table->hashtable, true, free_func);
	free(table->image_file_name);
}