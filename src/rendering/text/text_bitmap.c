#include "rendering/text/text_bitmap.h"
#include "surface.h"
#include "game_io.h"
#include "libft.h"

#include <stdio.h>

// Add support for dynamic font size

// @Cleanme Change the order of these variables and see if we can remove a few
// @TODO Color is currently not being rendered due to the input image being a static color.
// @TODO Need to change the color when loading the image. Make a function to do this so it can be done at anytime.
t_text_bitmap *new_text_bitmap(char *string, SDL_Renderer *renderer, char *bitmap_json_name, SDL_Color color, t_vector2i position)
{
	t_text_bitmap *text;
	char 		  *name;
	char          *path;

	// @Cleanme Add implementation for logger and fatal errors/asserts
	if (!renderer || !bitmap_json_name)
	{
		printf("ERROR: Renderer or font null!\n");
		return NULL;
	}
	if (!string || strcmp(string, "") == 0)
	{
		printf("ERROR: Creating text with empty string!\n");
		return NULL;
	}
	text = ft_memalloc(sizeof(t_text_bitmap));
	if (!text)
		return NULL;
	text = change_text_string_bitmap(text, string);
	if (!text)
		return NULL;
	if (init_glyph_table(&text->glyph_table, 0) != GLYPH_SUCCESS)
		return (NULL);
	if (set_glyph_table_values(&text->glyph_table, bitmap_json_name) != 0)
		return NULL;
    path = get_path_of_file(bitmap_json_name);
    if (path) {
        name = ft_strjoin(path, text->glyph_table.image_file_name);
        if (!name)
            return NULL;
        free(path);
    }
    else
        name = ft_strdup(text->glyph_table.image_file_name);
    text->bitmap = new_texture(renderer, name);
	free(name);
	if (!text->bitmap)
	{
		free_text_bitmap(text);
		return NULL;
	}
	text->color = color;
	text->position = position;
	return text;
}

t_text_bitmap *change_text_string_bitmap(t_text_bitmap *text, char *new)
{
	if (!text || !new)
	{
		printf("ERROR: Cannot change the text of a null text!\n");
		return NULL;
	}
	if (text->string)
		free(text->string);
	text->string = ft_strnew(ft_strlen(new));
	text->string = ft_strncpy(text->string, new, ft_strlen(new));
	return text;
}

void render_text_bitmap(t_text_bitmap *text, SDL_Renderer *renderer)
{
	t_vector2i pos;
	t_glyph_node *node = NULL;
	SDL_Rect src_rect;
	SDL_Rect dst_rect;
	glyph_table_error code;

	pos = text->position;
	for (int i = 0; text->string[i]; i++)
	{
		if (text->string[i] != '\n' && text->string[i] != ' ')
		{
			code = get_glyph_table_node(&text->glyph_table, text->string[i], &node);
			if (code == 0)
			{
				src_rect.x = node->pos.x;
				src_rect.y = node->pos.y;
				src_rect.w = node->size.x;
				src_rect.h = node->size.y;
				dst_rect.x = pos.x + node->offset.x + node->xadvance;
				dst_rect.y = pos.y + node->offset.y;
				dst_rect.w = node->size.x;
				dst_rect.h = node->size.y;
				SDL_RenderCopy(renderer, text->bitmap->texture, &src_rect, &dst_rect);
				pos.x += node->size.x;
			}
			else
			{
				printf("ERROR: Failed to get node for char %c with error code %d\n", text->string[i], code);
				return;
			}
			if (!((text->string[i] >= 65 && text->string[i] <= 90) || (text->string[i] >= 97 && text->string[i] <= 122)))
				pos.x += 5;
		}
		else if (text->string[i] == '\n')
		{
			pos.x = text->position.x;
			pos.y += text->glyph_table.font_size;
		}
		else if (text->string[i] == ' ')
			pos.x += text->glyph_table.font_size;
	}
}

void free_text_bitmap(t_text_bitmap *text)
{
	if (text->string)
		free(text->string);
	if (text->bitmap)
	    free_texture(text->bitmap);
	free_glyph_table(&text->glyph_table, NULL);
	free(text);
	text = NULL;
}