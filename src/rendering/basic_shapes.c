//
// Created by kohlten on 1/5/19.
//

#include "rendering/basic_shapes.h"
#include <SDL2_gfxPrimitives.h>

// @TODO Make a custom line drawing function instead of using SDL so that we can draw to a surface for increased performance

void draw_circle(t_vector2i center, int size, bool filled, SDL_Color color, SDL_Renderer *renderer) {
    int x = size - 1;
    int y = 0;
    int dx = 1;
    int dy = 1;
    int err = dx - (size << 1);

    SDL_SetRenderDrawColor(renderer, color.r, color.g, color.b, color.a);
    while (x >= y) {
        if (filled == false) {
            SDL_RenderDrawPoint(renderer, center.x + x, center.y + y);
            SDL_RenderDrawPoint(renderer, center.x + y, center.y + x);
            SDL_RenderDrawPoint(renderer, center.x - y, center.y + x);
            SDL_RenderDrawPoint(renderer, center.x - x, center.y + y);
            SDL_RenderDrawPoint(renderer, center.x - x, center.y - y);
            SDL_RenderDrawPoint(renderer, center.x - y, center.y - x);
            SDL_RenderDrawPoint(renderer, center.x + y, center.y - x);
            SDL_RenderDrawPoint(renderer, center.x + x, center.y - y);
        } else {
            for (int i = center.x - x; i <= center.x + x; i++) {
                SDL_RenderDrawPoint(renderer, i, center.y + y);
                SDL_RenderDrawPoint(renderer, i, center.y - y);
            }
            for (int i = center.x - y; i <= center.x + y; i++) {
                SDL_RenderDrawPoint(renderer, i, center.y + x);
                SDL_RenderDrawPoint(renderer, i, center.y - x);
            }
        }
        if (err <= 0) {
            y++;
            err += dy;
            dy += 2;
        }
        if (err > 0) {
            x--;
            dx += 2;
            err += dx - (size << 1);
        }
    }
}

void draw_rect(SDL_Renderer *renderer, t_vector2i pos, t_vector2i size, bool aa, short r, short g, short b, short a) {
    if (aa) {
        aalineRGBA(renderer, pos.x,          pos.y,          pos.x,          pos.y + size.y, r, g, b, a);
        aalineRGBA(renderer, pos.x,          pos.y + size.y, pos.x + size.x, pos.y + size.y, r, g, b, a);
        aalineRGBA(renderer, pos.x + size.x, pos.y + size.y, pos.x + size.x, pos.y,          r, g, b, a);
        aalineRGBA(renderer, pos.x + size.x, pos.y,          pos.x,          pos.y,          r, g, b, a);
    } else{
        lineRGBA(renderer,   pos.x,          pos.y,          pos.x,          pos.y + size.y, r, g, b, a);
        lineRGBA(renderer,   pos.x,          pos.y + size.y, pos.x + size.x, pos.y + size.y, r, g, b, a);
        lineRGBA(renderer,   pos.x + size.x, pos.y + size.y, pos.x + size.x, pos.y,          r, g, b, a);
        lineRGBA(renderer,   pos.x + size.x, pos.y,          pos.x,          pos.y,          r, g, b, a);
    }
}

