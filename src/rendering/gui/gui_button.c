//
// Created by kohlten on 1/15/19.
//

#include <SDL2_gfxPrimitives.h>

#include "rendering/gui/gui_button.h"
#include "rendering/basic_shapes.h"
#include "libft.h"

int init_gui_button(t_gui_button *button, t_text_collage *text, t_vector2i pos, t_vector2i button_size,
                    t_surface *regular, t_surface *hover, t_surface *pressed) {
    if (!button || !text)
        return -1;
    button->text = text;
    button->pos = pos;
    button->size = button_size;
    button->normal_surf = regular;
    button->hovering_surf = hover;
    button->clicked_surf = pressed;
    return 0;
}

int change_string_gui_button(t_gui_button *button, char *string) {
    button->string = ft_strdup(string);
    if (!button->string) return -1;
    return 0;
}

void update_gui_button(t_gui_button *button, t_vector2i mouse_pos, bool mouse_clicked) {
    if (mouse_pos.x <= button->pos.x + button->size.x && mouse_pos.x >= button->pos.x &&
        mouse_pos.y <= button->pos.y + button->size.y && mouse_pos.y >= button->pos.y) {
        if (!mouse_clicked)
            button->hovering = true;
        else
            button->clicked = true;
    } else {
        button->hovering = false;
        button->clicked = false;
    }
}

void draw_gui_button(t_gui_button *button, SDL_Renderer *renderer, double angle, double zoom, bool aa, bool debug) {
    t_vector2i text_size;
    t_vector2i pos;

    if (!button->string) return;
    if (debug)
        draw_rect(renderer, button->pos, button->size, aa, 255, 0, 0, 255);
    if (button->clicked_surf && button->clicked)
        rotate_and_draw_surface(button->clicked_surf, renderer, button->pos, angle, zoom, aa);
    else if (button->hovering_surf && button->hovering)
        rotate_and_draw_surface(button->hovering_surf, renderer, button->pos, angle, zoom, aa);
    else {
        if (button->normal_surf)
            rotate_and_draw_surface(button->normal_surf, renderer, button->pos, angle, zoom, aa);
    }
    text_size = get_text_size_text_collage(button->text, button->string, zoom);
    pos = new_vector2i((button->pos.x + (button->size.x / 2)) - text_size.x / 2,
                       (button->pos.y + (button->size.y / 2)) - text_size.y / 2);
    draw_text_collage(button->text, button->string, renderer, pos, angle, zoom, aa, NULL, 0);
}

void reset_gui_button(t_gui_button *button) {
    button->clicked = false;
    button->hovering = false;
}

void free_gui_button(t_gui_button *button) {
    if (button->string) free(button->string);
}