//
// Created by Alexander Strole on 12/11/18.
//

#include "rendering/gui/gui.h"
#include "game_io.h"
#include <jansson.h>
#include "libft.h"
#include "rendering/gui/gui_image.h"
#include "rendering/gui/gui_text.h"
#include "texture.h"

// @TODO Add Text and button
static gui_error parse_gui_image(t_gui *gui, json_t *object, SDL_Renderer *renderer) {
    t_gui_image *image;
    t_gui_node *g_node;
    json_t *node;
    t_texture *texture;
    char *filename;
    size_t i;

    image = ft_memalloc(sizeof(t_gui_image));
    if (!image) return GUI_MALLOC_ERROR;
    vector_init(&image->sprites);
    node = json_object_get(object, "position");
    if (!json_is_array(node)) return GUI_JSON_ERROR;
    image->position.x = json_integer_value(json_array_get(node, 0));
    image->position.y = json_integer_value(json_array_get(node, 1));
    node = json_object_get(object, "size");
    if (!json_is_array(node)) return GUI_JSON_ERROR;
    image->size.x = json_integer_value(json_array_get(node, 0));
    image->size.y = json_integer_value(json_array_get(node, 1));
    node = json_object_get(object, "sprites");
    if (!json_is_array(node)) return GUI_JSON_ERROR;
    for (i = 0; i < json_array_size(node); i++) {
        filename = (char *) json_string_value(json_array_get(node, i));
        if (get_node_hashtable(&gui->images, filename, (void **) &texture) != HASH_SUCCESS) {
            filename = ft_strjoin("../../gui_images/", filename);
            texture = new_texture(renderer, filename);
            if (!texture) return GUI_JSON_ERROR;
            if (add_node_hashtable(&gui->images, filename, texture) != HASH_SUCCESS) return GUI_JSON_ERROR;
            free(filename);
        }
        vector_add(&image->sprites, texture);
    }
    g_node = ft_memalloc(sizeof(t_gui_node));
    if (!g_node) return GUI_MALLOC_ERROR;
    g_node->node = image;
    g_node->type = GUI_IMAGE;
    vector_add(&gui->objects, g_node);
    return GUI_SUCCESS;
}

static gui_error parse_gui_text(t_gui *gui, json_t *object, SDL_Renderer *renderer) {
    //t_gui_text *text;
    //t_gui_node *g_node;
    //json_t *node;
    (void) gui;
    (void) object;
    (void) renderer;
    return GUI_SUCCESS;
}

static gui_error send_to_create_function(t_gui *gui, json_t *object, char *type, SDL_Renderer *renderer) {
    gui_error code;

    if (ft_strcmp(type, "image") == 0)
        code = parse_gui_image(gui, object, renderer);
    else if (ft_strcmp(type, "text") == 0)
        code = parse_gui_text(gui, object, renderer);
    else
        return GUI_UNKNOWN_TYPE;
    return code;
}

static gui_error parse_gui_values(t_gui *gui, json_t *root, SDL_Renderer *renderer) {
    json_t *node;
    json_t *object;
    json_t *type;
    gui_error code;
    size_t i;

    node = json_object_get(root, "objects");
    if (!json_is_array(node)) return GUI_JSON_ERROR;
    for (i = 0; i < json_array_size(node); i++) {
        object = json_array_get(node, i);
        if (!json_is_object(object)) return GUI_JSON_ERROR;
        type = json_object_get(object, "type");
        if (!json_is_string(type)) return GUI_JSON_ERROR;
        code = send_to_create_function(gui, object, (char *) json_string_value(type), renderer);
        if (code != GUI_SUCCESS) return code;
    }
    return GUI_SUCCESS;
}

gui_error init_gui(t_gui *gui, char *filename, SDL_Renderer *renderer) {
    char *text;
    json_t *root;
    json_error_t error;
    gui_error code;

    text = read_file(filename);
    if (!text)
        return GUI_FILE_ERROR;
    root = json_loads(text, 0, &error);
    if (!root) {
        printf("error: on line %d: %s\n", error.line, error.text);
        return GUI_JSON_ERROR;
    }
    free(text);
    init_hashtable(&gui->fonts, 0);
    init_hashtable(&gui->images, 0);
    vector_init(&gui->objects);
    code = parse_gui_values(gui, root, renderer);
    json_decref(root);
    return code;
}

void display_gui(t_gui *gui, SDL_Renderer *renderer) {
    int i;
    t_gui_node *g_node;

    for (i = 0; i < vector_total(&gui->objects); i++) {
        g_node = vector_get(&gui->objects, i);
        if (g_node->type == GUI_IMAGE)
            draw_gui_image(g_node->node, renderer);
    }
}