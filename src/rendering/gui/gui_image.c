//
// Created by Alexander Strole on 12/11/18.
//

#include "rendering/gui/gui_image.h"
#include "texture.h"


void draw_gui_image(t_gui_image *image, SDL_Renderer *renderer)
{
    render_texture(vector_get(&image->sprites, image->current), renderer, image->position, NULL, 0, new_vector2i(0, 0), SDL_FLIP_NONE);
}

void free_gui_image(t_gui_image *image)
{
    free(image);
}