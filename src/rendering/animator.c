//
// Created by kohlten on 12/16/18.
//

#include "rendering/animator.h"
#include "assert.h"
#include "libft.h"
#include "vector/vector.h"
#include "texture.h"
#include <SDL.h>

// @TODO Make these return errors stored in a enum

// @TODO Make this based off of time instead of frames
// @TODO Animator could be null here
int init_animator(t_animator *animator) {
    if (!animator || animator->inited == 1)
        return -1;
    animator->total_frames = 0;
    if (init_hashtable(&animator->states, 0) != HASH_SUCCESS)
        return -2;
    animator->current_state = NULL;
    animator->inited = 1;
    return 0;
}

// The textures vector should be a vector full of t_textures not SDL_Textures
int add_state_animator(t_animator *animator, char *state_name, vector *textures, int wait_time, int current_img, int max_times_played) {
    t_animator_state_node *new_node;

    if (!animator || !textures || !state_name || animator->inited != 1)
        return -1;
    new_node = ft_memalloc(sizeof(t_animator_state_node));
    assert(new_node != NULL, "Failed to malloc new animator node!\n");
    vector_init(&new_node->images);
    for (int i = 0; i < vector_total(textures); i++) {
        vector_add(&new_node->images, vector_get(textures, i));
    }
    assert(vector_total(&new_node->images) == vector_total(textures),
           "Failed to get all the images into the new vector!\n");
    new_node->current_img = current_img;
    new_node->wait_time = wait_time;
    new_node->max_times_played = max_times_played;
    new_node->playing = 0;
    new_node->frame_start_time = -1;
    if (add_node_hashtable(&animator->states, state_name, new_node) != HASH_SUCCESS)
        return -1;
    return 0;
}

// @TODO Appears like the total is not correct
// @TODO Use timer here instead
int update_animator(t_animator *animator) {
    t_animator_state_node *node;

    if (!animator || animator->inited != 1)
        return -1;
    if (animator->current_state != NULL) {
        if (get_node_hashtable(&animator->states, animator->current_state, (void **) &node) != HASH_SUCCESS)
            return -1;
        if (node->playing) {
            if ((int)(SDL_GetTicks() - node->frame_start_time) >= node->wait_time) {
                if (node->current_img >= vector_total(&node->images) - 1) {
                    node->current_img = 0;
                    node->times_played++;
                }
                node->current_img++;
                node->frame_start_time = SDL_GetTicks();
            }
            if (node->times_played > node->max_times_played && node->max_times_played != -1)
                node->playing = 0;
        }
    } else
        return -1;
    animator->total_frames++;
    return 0;
}

int draw_animator(t_animator *animator, SDL_Renderer *renderer, t_vector2i pos, double angle, double zoom, bool smooth) {
    t_animator_state_node *node;
    t_surface *current;

    if (!animator || animator->inited != 1)
        return -1;
    if (animator->current_state != NULL) {
        if (get_node_hashtable(&animator->states, animator->current_state, (void **) &node) != HASH_SUCCESS)
            return -1;
        current = vector_get(&node->images, node->current_img);
        if (!current)
            return -1;
        return rotate_and_draw_surface(current, renderer, pos, angle, zoom, smooth);
    } else
        return -1;
    return 0;
}

int change_state_animator(t_animator *animator, char *state_name) {
    if (!animator || !state_name || animator->inited != 1)
        return -1;
    if (!node_in_hashtable(&animator->states, state_name))
        return -1;
    if (animator->current_state != NULL)
        free(animator->current_state);
    animator->current_state = ft_strdup(state_name);
    return 0;
}

int get_frame_animator(t_animator *animator) {
    t_animator_state_node *node;

    if (!animator)
        return -1;
    if (!animator->current_state)
        return -1;
    if (get_node_hashtable(&animator->states, animator->current_state, (void **) &node) != HASH_SUCCESS)
        return -1;
    if (node->playing)
        return node->current_img;
    else
        return -1;
}

char *get_current_state_animator(t_animator *animator) {
    if (animator->current_state)
        return animator->current_state;
    return NULL;
}

t_surface *get_current_image_animator(t_animator *animator) {
    t_animator_state_node *node;

    if (!animator)
        return NULL;
    if (!animator->current_state)
        return NULL;
    if (get_node_hashtable(&animator->states, animator->current_state, (void **) &node) != HASH_SUCCESS)
        return NULL;
    return vector_get(&node->images, node->current_img);
}

int get_num_frames_animator(t_animator *animator) {
    t_animator_state_node *node;

    if (!animator)
        return -1;
    if (!animator->current_state)
        return -1;
    if (get_node_hashtable(&animator->states, animator->current_state, (void **) &node) != HASH_SUCCESS)
        return -1;
    return vector_total(&node->images);
}

t_vector2i get_frame_size(t_animator *animator) {
    t_animator_state_node *node;
    t_texture *tex;

    if (!animator)
        return new_vector2i(-1, -1);
    if (!animator->current_state)
        return new_vector2i(-1, -1);
    if (get_node_hashtable(&animator->states, animator->current_state, (void **) &node) != HASH_SUCCESS)
        return new_vector2i(-1, -1);
    if (node->current_img < 0 || node->current_img > vector_total(&node->images) - 1)
        return  new_vector2i(-1, -1);
    tex = vector_get(&node->images, node->current_img);
    return new_vector2i(tex->width, tex->height);
}


int start_playing_animator(t_animator *animator) {
    t_animator_state_node *node;

    if (!animator || animator->inited != 1)
        return -1;
    if (animator->current_state != NULL) {
        if (get_node_hashtable(&animator->states, animator->current_state, (void **) &node) != HASH_SUCCESS)
            return -1;
        node->playing = 1;
        node->times_played = 0;
        node->frame_start_time = SDL_GetTicks();
    } else
        return -1;
    return 0;
}

int set_frame_animator(t_animator *animator, int image, char *state_name) {
    t_animator_state_node *node;

    if (!animator || animator->inited != 1)
        return -1;
    if (state_name == NULL) {
        if (animator->current_state == NULL)
            return -1;
        else
            state_name = animator->current_state;
    }
    if (get_node_hashtable(&animator->states, state_name, (void **) &node) != HASH_SUCCESS)
        return -1;
    if (image > vector_total(&node->images) - 1)
        return -1;
    node->current_img = image;
    return 0;
}

int stop_playing_animator(t_animator *animator) {
    t_animator_state_node *node;

    if (!animator || animator->inited != 1)
        return -1;
    if (animator->current_state != NULL) {
        if (get_node_hashtable(&animator->states, animator->current_state, (void **) &node) != HASH_SUCCESS)
            return -1;
        node->playing = 0;
        node->frame_start_time = -1;
    } else
        return -1;
    return 0;
}

bool is_playing_animator(t_animator *animator) {
    t_animator_state_node *node;

    if (!animator || animator->inited != 1)
        return false;
    if (animator->current_state != NULL) {
        if (get_node_hashtable(&animator->states, animator->current_state, (void **) &node) != HASH_SUCCESS)
            return false;
        return node->playing;
    } else
        return false;
}

static void free_animator_node_textures(void *p) {
    t_animator_state_node *node;

    node = (t_animator_state_node *) p;
    for (int i = 0; i < vector_total(&node->images); i++)
        free_texture(vector_get(&node->images, i));
    vector_free(&node->images);
    free(node);
}

static void free_animator_node(void *p) {
    t_animator_state_node *node;

    node = (t_animator_state_node *) p;
    vector_free(&node->images);
    free(node);
}

void free_animator(t_animator *animator, bool free_textures) {
    if (!animator)
        return;
    if (free_textures)
        free_hashtable(&animator->states, 1, free_animator_node_textures);
    else
        free_hashtable(&animator->states, 1, free_animator_node);
    if (animator->current_state)
        free(animator->current_state);
    animator->inited = 0;
}
