//
// Created by kohlten on 1/17/19.
//

#include "vector/vector.h"
#include <math.h>

#define MAX_STEPS 50

bool ray_intersects_pos(t_vector2f pos, t_vector2f dir, t_vector2f point, double dist, t_vector2f *instersect_point) {
    t_vector2f pixel_pos;
    t_vector2f sideDist;
    t_vector2f deltaDist;
    t_vector2f step;
    int steps = 0;
    bool hit = false;

    pixel_pos = new_vector2f(pos.x, pos.y);
    deltaDist = new_vector2f(fabs(1 / dir.x), fabs(1 / dir.y));
    if (dir.x < 0) {
        step.x = -1;
        sideDist.x = (pos.x - pixel_pos.x) * deltaDist.x;
    } else {
        step.x = 1;
        sideDist.x = (pixel_pos.x + 1.0 - pos.x) * deltaDist.x;
    }
    if (dir.y < 0) {
        step.y = -1;
        sideDist.y = (pos.y - pixel_pos.x) * deltaDist.y;
    } else {
        step.y = 1;
        sideDist.y = (pixel_pos.y + 1.0 - pos.y) * deltaDist.y;
    }
    while (!hit && steps < MAX_STEPS) {
        if (sideDist.x < sideDist.y) {
            sideDist.x += deltaDist.x;
            pixel_pos.x += step.x;
        } else {
            sideDist.y += deltaDist.y;
            pixel_pos.y += step.y;
        }
        if (dist_vector(pixel_pos, point) <= dist) hit = 1;
        steps++;
    }
    if (hit){
        if (instersect_point)
            *instersect_point = pixel_pos;
        return true;
    }
    else
        return false;
}

