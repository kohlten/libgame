//
// Created by Alexander Strole on 12/2/18.
//

#include "vector/physics_object.h"
#include "libft.h"

void init_physics_object(t_physics_object *object)
{
    object->pos = vector2f_zero;
    object->vel = vector2f_zero;
    object->acc = vector2f_zero;
}

void apply_force(t_physics_object *object, t_vector2f force)
{
    object->acc = add_vector2f(object->acc, force);
}

void update_object(t_physics_object *object)
{
    object->vel = add_vector2f(object->vel, object->acc);
    object->acc = vector2f_zero;
    object->pos = add_vector2f(object->vel, object->pos);
}

void change_pos_object(t_physics_object *object, t_vector2f new_pos)
{
    object->pos = new_pos;
}

void change_vel_object(t_physics_object *object, t_vector2f new_vel)
{
    object->vel = new_vel;
}