//
// Created by kohlten on 1/3/19.
//

#include "vector/collisions.h"

bool point_in_circle(t_vector2f point, t_vector2f circle_pos, double circle_radius) {
    if (dist_vector(point, circle_pos) <= circle_radius)
        return true;
    return false;
}

bool line_in_circle(t_vector2f line_start, t_vector2f line_end, t_vector2f circle_center, double circle_radius) {
    t_vector2f seg_v;
    t_vector2f pt_v;
    t_vector2f proj_v;
    t_vector2f seg_v_unit;
    t_vector2f closest;
    t_vector2f dist_v;
    double proj;

    seg_v = sub_vector2f(line_end, line_start);
    pt_v = sub_vector2f(circle_center, line_start);
    seg_v_unit = div_by_vector2f(seg_v, mag_vector(seg_v));
    proj = dot_vector(pt_v, seg_v_unit);
    if (proj <= 0)
        closest = line_start;
    else if (proj >= mag_vector(seg_v))
        closest = line_end;
    else {
        proj_v = mul_by_vector2f(seg_v_unit, proj);
        closest = add_vector2f(proj_v, line_start);
    }
    dist_v = sub_vector2f(circle_center, closest);
    if (mag_vector(dist_v) <= 0 || mag_vector(dist_v) <= circle_radius)
        return true;
    return false;
}

bool triangle_in_circle(t_vector2f p1, t_vector2f p2, t_vector2f p3, t_vector2f circle_center, double circle_radius) {
    if (point_in_circle(p1, circle_center, circle_radius) ||
        point_in_circle(p2, circle_center, circle_radius) ||
        point_in_circle(p3, circle_center, circle_radius))
        return true;
    if (line_in_circle(p1, p2, circle_center, circle_radius) ||
        line_in_circle(p2, p3, circle_center, circle_radius)  ||
        line_in_circle(p3, p1, circle_center, circle_radius))
        return true;
    return false;
}

