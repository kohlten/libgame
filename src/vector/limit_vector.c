//
// Created by Alexander Strole on 12/2/18.
//

#include "vector/vector.h"

t_vector2f limit_vector2f(t_vector2f vec, double limit)
{
    double magsqrt;

    magsqrt = magsqrt_vector(vec);
    if (magsqrt > limit * limit)
    {
        vec = div_by_vector2f(vec, sqrt(magsqrt));
        vec = mul_by_vector2f(vec, limit);
    }
    return vec;
}

t_vector2i limit_vector2i(t_vector2i vec, double limit)
{
    double magsqrt;

    magsqrt = magsqrt_vector(vec);
    if (magsqrt > limit * limit)
    {
        vec = div_by_vector2i(vec, sqrt(magsqrt));
        vec = mul_by_vector2i(vec, limit);
    }
    return vec;
}

t_vector2u limit_vector2u(t_vector2u vec, double limit)
{
    double magsqrt;

    magsqrt = magsqrt_vector(vec);
    if (magsqrt > limit * limit)
    {
        vec = div_by_vector2u(vec, sqrt(magsqrt));
        vec = mul_by_vector2u(vec, limit);
    }
    return vec;
}

t_vector2l limit_vector2l(t_vector2l vec, double limit)
{
    double magsqrt;

    magsqrt = magsqrt_vector(vec);
    if (magsqrt > limit * limit)
    {
        vec = div_by_vector2l(vec, sqrt(magsqrt));
        vec = mul_by_vector2l(vec, limit);
    }
    return vec;
}

t_vector2ul limit_vector2ul(t_vector2ul vec, double limit)
{
    double magsqrt;

    magsqrt = magsqrt_vector(vec);
    if (magsqrt > limit * limit)
    {
        vec = div_by_vector2ul(vec, sqrt(magsqrt));
        vec = mul_by_vector2ul(vec, limit);
    }
    return vec;
}
