//
// Created by Alexander Strole on 12/2/18.
//

#include "vector/vector.h"


t_vector2i rotate_vector2i(t_vector2i vec, t_vector2i origin, double angle)
{
    t_vector2i new;

    vec = sub_vector2i(vec, origin);
    new.x = vec.x * cos(angle) - vec.y * sin(angle);
    new.y = vec.x * sin(angle) + vec.y * cos(angle);
    new = add_vector2i(new, origin);
    return new;
}

t_vector2u rotate_vector2u(t_vector2u vec, t_vector2u origin, double angle)
{
    t_vector2u new;

    vec = sub_vector2u(vec, origin);
    new.x = vec.x * cos(angle) - vec.y * sin(angle);
    new.y = vec.x * sin(angle) + vec.y * cos(angle);
    new = add_vector2u(new, origin);
    return new;
}

t_vector2l rotate_vector2l(t_vector2l vec, t_vector2l origin, double angle)
{
    t_vector2l new;

    vec = sub_vector2l(vec, origin);
    new.x = vec.x * cos(angle) - vec.y * sin(angle);
    new.y = vec.x * sin(angle) + vec.y * cos(angle);
    new = add_vector2l(new, origin);
    return new;
}

t_vector2ul rotate_vector2ul(t_vector2ul vec, t_vector2ul origin, double angle)
{
    t_vector2ul new;

    vec = sub_vector2ul(vec, origin);
    new.x = vec.x * cos(angle) - vec.y * sin(angle);
    new.y = vec.x * sin(angle) + vec.y * cos(angle);
    new = add_vector2ul(new, origin);
    return new;
}

t_vector2f rotate_vector2f(t_vector2f vec, t_vector2f origin, double angle)
{
    t_vector2f new;

    vec = sub_vector2f(vec, origin);
    new.x = vec.x * cos(angle) - vec.y * sin(angle);
    new.y = vec.x * sin(angle) + vec.y * cos(angle);
    new = add_vector2f(new, origin);
    return new;
}
