//
// Created by Alexander Strole on 12/1/18.
//

#include "vector/vector.h"

t_vector2f norm_vector2f(t_vector2f vec)
{
    double len;

    len = mag_vector(vec);
    if (len != 0)
        return mul_by_vector2f(vec, 1 / len);
    else
        return vec;
}

t_vector2i norm_vector2i(t_vector2i vec)
{
    double len;

    len = mag_vector(vec);
    if (len != 0)
        return mul_by_vector2i(vec, 1 / len);
    else
        return vec;
}

t_vector2u norm_vector2u(t_vector2u vec)
{
    double len;

    len = mag_vector(vec);
    if (len != 0)
        return mul_by_vector2u(vec, 1 / len);
    else
        return vec;
}

t_vector2l norm_vector2l(t_vector2l vec)
{
    double len;

    len = mag_vector(vec);
    if (len != 0)
        return mul_by_vector2l(vec, 1 / len);
    else
        return vec;
}

t_vector2ul norm_vector2ul(t_vector2ul vec)
{
    double len;

    len = mag_vector(vec);
    if (len != 0)
        return mul_by_vector2ul(vec, 1 / len);
    else
        return vec;
}

