//
// Created by Alexander Strole on 12/2/18.
//

#include "vector/vector.h"

double	constrain(double n, double low, double high)
{
    return (fmax(fmin(n, high), low));
}

double	map(double n, t_vector2f range1, t_vector2f range2)
{
    double newval;

    newval = (n - range1.x) /
             (range1.y - range1.x) * (range2.y - range2.x) + range2.x;
    return (newval);
}

double	map_constrain(double n, t_vector2f range1, t_vector2f range2)
{
    double newval;

    newval = (n - range1.x) /
             (range1.y - range1.x) * (range2.y - range2.x) + range2.x;
    if (range2.x < range2.y)
        return (constrain(newval, range2.x, range2.y));
    else
        return (constrain(newval, range2.y, range2.x));
}
