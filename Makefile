OUT = build/libgame.a

CFILES := $(shell find src | grep "\\.c" | sed "s/\.\///g")

OFILES := $(patsubst %.c, build/obj/%.o, $(CFILES))

OFILE_PATH := $(notdir $(OFILES))
OFILE_PATH := $(patsubst %.o, build/obj/%.o, $(OFILE_PATH))

JANSSON := $(shell pkg-config jansson --cflags)
SDL2IMAGE := $(shell pkg-config SDL2_image --cflags)
SDL2TTF := $(shell pkg-config SDL2_ttf --cflags)
SDL2 := $(shell pkg-config sdl2 --cflags)
SODIUM := $(shell pkg-config libsodium --cflags)
GLEW := $(shell pkg-config glew --cflags)

CC = gcc

CFLAGS =-g -Wall -Wextra -Werror -I ../libgame/include -I ../libft/include $(JANSSON) $(SDL2IMAGE) $(SDL2TTF) $(SDL2) $(SODIUM) $(GLEW)

all: build $(OFILES)
	ar rc $(OUT) $(OFILE_PATH)
	ranlib $(OUT)

build:
	-@mkdir build
	-@mkdir build/obj

build/obj/%.o: %.c
	@echo "CC $(CFLAGS) -c -o build/obj/$(notdir $@) $<"
	@$(CC) $(CFLAGS) -c -o build/obj/$(notdir $@) $< $(LIBS)

test:
	cd ../tests && make && make test

clean:
	rm -rf build/obj

fclean: clean
	rm -rf $(OUT)
	rm -rf build

re: clean all

.PHONY: all fclean clean re test build
